# Issues

On my system jsonccp causes a memory leak refer to the followng issue: https://github.com/open-source-parsers/jsoncpp/issues/507  

The output of valgrind gives this when run on a simple test program

    ==11533== 
    ==11533== HEAP SUMMARY:
    ==11533==     in use at exit: 40 bytes in 1 blocks
    ==11533==   total heap usage: 1,539 allocs, 1,538 frees, 179,667 bytes allocated
    ==11533== 
    ==11533== 40 bytes in 1 blocks are still reachable in loss record 1 of 1
    ==11533==    at 0x4C3017F: operator new(unsigned long) (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
    ==11533==    by 0x4E55B89: Json::Value::nullSingleton() (in /usr/lib/x86_64-linux-gnu/libjsoncpp.so.1.7.4)
    ==11533==    by 0x4E48348: ??? (in /usr/lib/x86_64-linux-gnu/libjsoncpp.so.1.7.4)
    ==11533==    by 0x4010782: call_init (dl-init.c:72)
    ==11533==    by 0x4010782: _dl_init (dl-init.c:119)
    ==11533==    by 0x40010C9: ??? (in /lib/x86_64-linux-gnu/ld-2.27.so)
    ==11533== 
    ==11533== LEAK SUMMARY:
    ==11533==    definitely lost: 0 bytes in 0 blocks
    ==11533==    indirectly lost: 0 bytes in 0 blocks
    ==11533==      possibly lost: 0 bytes in 0 blocks
    ==11533==    still reachable: 40 bytes in 1 blocks
    ==11533==         suppressed: 0 bytes in 0 blocks
    ==11533== 
    ==11533== For counts of detected and suppressed errors, rerun with: -v
    ==11533== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
