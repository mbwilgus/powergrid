#ifndef __PRESENTABLE_H__
#define __PRESENTABLE_H__

#include <string>

class Presentable
{
  public:
    virtual ~Presentable();

    virtual void show() = 0;
};

#endif
