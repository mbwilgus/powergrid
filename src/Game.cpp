#include "Game.h"

#include "AuctionHouse.h"
#include "AuctionHouseView.h"
#include "Card.h"
#include "Cart.h"
#include "Definition.h"
#include "Map.h"
#include "Market.h"
#include "Player.h"
#include "PowerplantMarketView.h"
#include "ResourceMarketView.h"
#include "Roster.h"
#include "Stockpile.h"
#include "Validator.h"

#include <deque>
#include <string>
#include <tuple>
#include <vector>

Game::Game() : escrow(nullptr), auctionHouse(nullptr), refinery(nullptr) {}

Game::Game(std::vector<Player*>& players,
           /* Map& map, */ PowerplantMarket* escrow, AuctionHouse* auctionHouse,
           ResourceMarket* refinery)
    : players(players), roster(new Roster(players)),
      /* map(map), */ escrow(escrow), auctionHouse(auctionHouse),
      refinery(refinery)
{
    escrowView = new PowerplantMarketView(escrow, roster);
    auctionHouseView = new AuctionHouseView(auctionHouse, roster);
    refineryView = new ResourceMarketView(refinery, roster);
}

Game::~Game()
{
    delete refineryView;
    delete auctionHouseView;
    delete escrowView;
    delete refinery;
    delete auctionHouse;
    delete escrow;
    delete roster;
}

PowerplantCard* Game::onPowerplantSelected()
{
    Player* current = roster->current();

    size_t selection;
    size_t limit = escrow->checkStock();
    bool mustBuy = current->numberOfPlantsOwned() == 0;

    SelectPowerplantValidator validator(selection, limit, mustBuy);

    do {
        escrowView->show();
    } while (escrowView->input(&validator));

    return escrow->select(selection);
}

void Game::onBidMade()
{
    Player* current = roster->current();

    size_t bid;
    size_t min;
    bool mustBid = auctionHouse->isOpeningBid();

    if (mustBid) {
        min = auctionHouse->getAuctioning()->getNumber();
    } else {
        min = auctionHouse->getCurrentBid() + 1;
    }

    size_t max = current->getElektro();

    BidValidator validator(bid, min, max, mustBid);

    do {
        auctionHouseView->show();
    } while (auctionHouseView->input(&validator));

    auctionHouse->placeBid(bid);
}

void Game::onPurchaseResources()
{
    Player* current = roster->current();

    size_t amount;
    ResourceQuota quotas = current->getResourceQuotas();

    ResourcePurchaseValidator validator(amount, quotas);

    do {
        refineryView->show();
    } while (refineryView->input(&validator));
}

void Game::phase2()
{
    // All palyers are conetestants in every auction by default.
    // Copy the current plauer lineup to refelct this.
    roster->save();

    // Keep auctioning plants until every player is satisfied.
    while (!roster->played()) {

        Player* auctionee = roster->next();

        escrow->notify(); // JUST FOR NOW

        // Players select plants in turn
        PowerplantCard* powerplant = onPowerplantSelected();

        // If the current player decided to start and auction... do so.
        if (powerplant) {

            // NOTE: Staring an auction changes the meaning of
            // Roster::played();
            auctionHouse->startAuction(roster, powerplant);

            while (!roster->played()) {
                /* Player* bidder = roster->current(); */
                onBidMade();
                roster->next();
            }

            PowerplantSale* sale;
            Player* winner;
            std::tie(sale, winner) = auctionHouse->declareWinner();

            auctionHouseView->announceWinner(powerplant, winner);

            // Finish the sale
            escrow->ringup(sale);
            escrow->checkout(winner);

            // The winner cannot participate in future auctions
            roster->bench(winner);
        } else {
            // Player decided to not auction a card and they cannot participate
            // in future auctions
            roster->bench(auctionee);
        }
    }

    roster->restore();
    roster->reverse();
}

void Game::phase3()
{
    Player* buyer = roster->next();

    while (!roster->played()) {
        Cart* cart = new Cart();
        refinery->peruse(cart);

        onPurchaseResources();

        ResourceSale* sale = cart->makeSale();

        refinery->ringup(sale);
        refinery->checkout(roster->current());

        buyer = roster->next();
    }

    roster->reverse();
}
