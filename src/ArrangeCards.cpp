#include "ArrangeCards.h"
#include "Card.h"
#include "ResourceType.h"

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <ios>
#include <iterator>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

namespace
{
std::string str_repeat(const std::string& str, size_t n)
{
    std::stringstream out;
    for (size_t i = 0; i < n; ++i) {
        out << str;
    }
    return out.str();
}
} // namespace

void ArrangeCards::addCard(const PowerplantCard* powerplant)
{
    std::string number = std::to_string(powerplant->getNumber());
    fieldWidth         = std::max(number.length(), fieldWidth);
    rows[0].push_back(number);

    std::string needed;
    if (powerplant->getNeeded() == 0) {
        needed = "eco";
    } else {
        needed = std::to_string(powerplant->getNeeded());
    }
    fieldWidth = std::max(needed.length(), fieldWidth);
    rows[1].push_back(needed);

    std::string powers = std::to_string(powerplant->getPowerable());
    fieldWidth         = std::max(powers.length(), fieldWidth);
    rows[2].push_back(powers);

    std::string resources = "";
    for (const ResourceType& resource : *powerplant) {
        resources += resource.name();
        if (resource != *std::prev(powerplant->end())) {
            resources += ' ';
        }
    }
    fieldWidth = std::max(resources.length(), fieldWidth);
    rows[3].push_back(resources);

    contentWidth = fieldLabelWidth + fieldWidth;
}

void ArrangeCards::processLabels()
{
    size_t cardWidth = contentWidth + (useBorder ? 2 : 0);

    ColumnProperties defaultColumnProperties =
        std::make_tuple("", "", contentWidth - 1, defaultSerperatorWidth);

    if (useLabels) {
        for (size_t i = 0; i < size; i++) {
            std::string labelText = "";

            try {
                labelText = _labels.at(i); // Check to see if exists
            } catch (std::out_of_range& e) {
            }

            if (labelText == "") {
                columnProperties.push_back(defaultColumnProperties);
                continue;
            }

            size_t frontPad = useBorder ? 2 : 1;

            std::string label;
            std::string labelOverflow = "";
            size_t labelWidth         = labelText.length() + 2;

            long overflow = (long)labelWidth - ((long)cardWidth - frontPad);
            size_t labelPadding   = 0;
            size_t seperatorWidth = defaultSerperatorWidth;

            if (overflow < 0) {
                label        = "[" + labelText + "]";
                labelPadding = -overflow;
            } else if (overflow - defaultSerperatorWidth > 0) {
                label = "[" + labelText.substr(0, cardWidth - frontPad - 1);
                labelOverflow =
                    labelText.substr(cardWidth - frontPad - 1) + "]";
                seperatorWidth = overflow + 1;
            }

            columnProperties.push_back(std::make_tuple(
                label, labelOverflow, labelPadding, seperatorWidth));
        }
    } else {
        columnProperties.resize(size);
        std::fill_n(columnProperties.begin(), size, defaultColumnProperties);
        /* for (size_t i = 0; i < size; i++) { */
        /*     columnProperties.push_back(defaultColumnProperties); */
        /* } */
    }
}

void ArrangeCards::processSeperators()
{
    size_t limit = std::min(_seperators.size(), size - 1);

    for (size_t i = 0; i < limit; ++i) {
        auto& serperator      = _seperators[i];
        size_t seperatorWidth = 0;

        for (std::string& row : serperator) {
            seperatorWidth = std::max(row.length() + 2, seperatorWidth);
        }

        size_t& currentSeperatorWidth = std::get<3>(columnProperties[i]);
        currentSeperatorWidth = std::max(seperatorWidth, currentSeperatorWidth);
    }
}

std::string ArrangeCards::getSeperatorString(size_t i, size_t j) const
{
    std::string seperatorText = "";
    size_t numSpaces          = 0;

    try {
        seperatorText         = _seperators.at(i).at(j);
        size_t seperatorWidth = std::get<3>(columnProperties[i]);
        numSpaces = std::floor((seperatorWidth - seperatorText.length()) / 2);
    } catch (const std::out_of_range& e) {
    }

    return std::string(numSpaces, ' ') + seperatorText;
}

void ArrangeCards::renderTopBorder(std::stringstream& builder)
{
    if (useBorder) {
        for (size_t i = 0; i < size; ++i) {
            size_t seperatorWidth;
            std::tie(std::ignore, std::ignore, std::ignore, seperatorWidth) =
                columnProperties[i];

            builder << cornerTopLeft << str_repeat(borderTop, contentWidth)
                    << cornerTopRight;
            builder.width(seperatorWidth);
            builder << "";
        }
    }

    builder << "\n";
}

void ArrangeCards::renderContent(std::stringstream& builder)
{
    for (size_t i = 0; i < rows.size(); ++i) {
        auto& row = rows[i];
        size_t j  = 0;

        for (const std::string& field : row) {
            std::string fieldLabel;

            std::ios_base& (*align)(std::ios_base&) = std::right;
            /* std::function<std::ios_base&(std::ios_base&)> align = std::right;
             */

            switch (i) {
            case 0: fieldLabel = "\u26EE : "; break;
            case 1: fieldLabel = "\u2301 : "; break;
            case 2: fieldLabel = "\u2302 : "; break;
            case 3:
                fieldLabel = "\u26fd\ufe0e : ";
                align      = std::left;
                break;
            }

            size_t seperatorWidth;
            std::tie(std::ignore, std::ignore, std::ignore, seperatorWidth) =
                columnProperties[j];

            builder << (useBorder ? borderLeft : "") << fieldLabel;
            builder.width(fieldWidth);
            builder << align << field << (useBorder ? borderRight : "");
            builder.width(seperatorWidth);
            builder << std::left << getSeperatorString(j++, i);
        }
        if (i != rows.size() - 1)
            builder << "\n";
    }
}

void ArrangeCards::renderBottomBorder(std::stringstream& builder)
{
    if (useBorder || useLabels) {
        builder << "\n";

        for (size_t i = 0; i < size; ++i) {
            std::string label;
            std::string labelOverflow;
            size_t labelPadding;
            size_t seperatorWidth;

            std::tie(label, labelOverflow, labelPadding, seperatorWidth) =
                columnProperties[i];

            builder << (useBorder ? cornerBottomLeft : "")
                    << (useBorder ? borderBottom : " ");
            builder.width(labelPadding);
            builder << (useBorder ? str_repeat(borderBottom, labelPadding)
                                  : "");
            builder << (label != "" ? label : cornerBottomRight);
            builder.width(seperatorWidth);
            builder << labelOverflow;
        }
    }
}

void ArrangeCards::render()
{
    columnProperties.clear();

    std::stringstream builder;

    processLabels();
    processSeperators();

    renderTopBorder(builder);
    renderContent(builder);
    renderBottomBorder(builder);

    cards = builder.str();
}

void ArrangeCards::border(bool use) { useBorder = use; }

void ArrangeCards::labels(std::vector<std::string>& labels)
{
    _labels       = labels;
    useLabels     = true;
    needsRerender = true;
}

void ArrangeCards::labels(std::vector<std::string>&& labels)
{
    _labels       = std::move(labels);
    useLabels     = true;
    needsRerender = true;
}

void ArrangeCards::seperators(
    std::vector<std::array<std::string, 4>>& serperators)
{
    _seperators   = serperators;
    needsRerender = true;
}

void ArrangeCards::operator()(const PowerplantCard* powerplant)
{
    ++size;
    addCard(powerplant);
    render();
    needsRerender = false;
}

void ArrangeCards::operator()(const PowerplantCard* powerplant1,
                              const PowerplantCard* powerplant2)
{
    size += 2;
    addCard(powerplant1);
    addCard(powerplant2);
    render();
    needsRerender = false;
}

std::ostream& operator<<(std::ostream& out, ArrangeCards& rhs)
{
    if (rhs.needsRerender) {
        rhs.render();
        rhs.needsRerender = false;
    }

    out << rhs.cards;
    return out;
}
