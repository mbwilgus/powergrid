#ifndef __RESOURCE_TYPE_H__
#define __RESOURCE_TYPE_H__

#include "Log.h"

#include <functional> // For hash
#include <iterator>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

class ResourceType;

template <> struct std::hash<ResourceType> {
    size_t operator()(const ResourceType& type) const noexcept;
};

class ResourceType
{
  private:
    size_t _index;
    std::string _name;

  public:
    ResourceType();
    ResourceType(size_t index, const std::string& name);
    const std::string& name() const;

    bool operator==(const ResourceType& rhs) const;
    bool operator!=(const ResourceType& rhs) const;
    bool operator<(const ResourceType& rhs) const;

    friend size_t std::hash<ResourceType>::operator()(
        const ResourceType& type) const noexcept;
};

class InvalidResourceError : public std::runtime_error
{
  public:
    InvalidResourceError(const char* msg);
};

class ResourceTypesBuilder
{
  public:
    static void initializeResources(const std::string& filename,
                                    const Log* logger);
};

enum class Property { poolSize, initialSupply, volatility, scarcityThreshold };

class ResourceTypes
{
  private:
    static size_t _items;
    static std::vector<ResourceType> _types;
    // Do I need this?
    static std::unordered_map<std::string, ResourceType> _resourceMap;
    static std::unordered_map<ResourceType,
                              std::unordered_map<Property, size_t>>
        resourceProperties;

  protected:
    ResourceTypes();

  public:
    class ResourceTypeIterator
        : public std::iterator<std::forward_iterator_tag, const ResourceType>
    {
      private:
        const ResourceType* _ptr;

      public:
        ResourceTypeIterator(const ResourceType* ptr) : _ptr(ptr) {}
        ResourceTypeIterator(const ResourceTypeIterator& source)
            : _ptr(source._ptr)
        {
        }
        ResourceTypeIterator& operator++()
        {
            ++_ptr;
            return *this;
        }
        ResourceTypeIterator operator++(int)
        {
            ResourceTypeIterator tmp(*this);
            operator++();
            return tmp;
        }
        bool operator==(const ResourceTypeIterator& rhs) const
        {
            return _ptr == rhs._ptr;
        }
        bool operator!=(const ResourceTypeIterator& rhs) const
        {
            return _ptr != rhs._ptr;
        }
        const ResourceType& operator*() { return *_ptr; }
        const ResourceType* operator->() { return _ptr; }
        ResourceTypeIterator operator-(const int rhs) { return ResourceTypeIterator(_ptr - rhs); }
    };

    static size_t size();
    static const ResourceTypes enumerate();
    // Do I need this
    static const ResourceType& get(const std::string& name);
    static size_t getProperty(const ResourceType& resource,
                              const Property property);
    static std::vector<std::pair<ResourceType, ResourceType>>
    getResourcePairs(const std::vector<ResourceType>& types);
    static ResourceTypeIterator begin();
    static ResourceTypeIterator end();

    friend class ResourceTypesBuilder;
};

#endif
