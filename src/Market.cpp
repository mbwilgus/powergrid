#include "Market.h"
#include "ArrangeCards.h"
#include "Buyable.h"
#include "Buyer.h"
#include "Card.h"
#include "Cart.h"
#include "Commodity.h"
#include "Controller.h"
#include "Deck.h"
#include "Definition.h"
#include "Log.h"
#include "ResourceType.h"

#include <algorithm> // For copy, sort
#include <array>
#include <cmath>   // For fmin
#include <cstddef> // For size_t
#include <fstream>
#include <iostream>
#include <ostream>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

Sale::Sale(Buyable* item, const Elektro price) : item(item), price(price) {}

Sale::Sale(const Sale& source) : item(source.item->clone()), price(source.price)
{
}

Sale::Sale(Sale&& source)
    : item(std::move(source.item)), price(std::move(source.price))
{
}

Sale::~Sale() {}

Buyable* Sale::claim(Buyer* buyer)
{
    buyer->spend(price);
    return item;
}

Sale& Sale::operator=(Sale source)
{
    swap(*this, source);
    return *this;
}

PowerplantSale::PowerplantSale(PowerplantCard* powerplant, Elektro price)
    : Sale(powerplant, price)
{
}

PowerplantSale::PowerplantSale(const PowerplantSale& source) : Sale(source) {}

PowerplantSale::PowerplantSale(PowerplantSale&& source)
    : Sale(std::move(source))
{
}

PowerplantSale::~PowerplantSale() {}

PowerplantCard* PowerplantSale::claim(Buyer* buyer)
{
    return static_cast<PowerplantCard*>(Sale::claim(buyer));
}

PowerplantSale& PowerplantSale::operator=(const PowerplantSale& source)
{
    Sale::operator=(source);
    return *this;
}

ResourceParcel::ResourceParcel() {}

ResourceParcel::~ResourceParcel() {}

ResourceParcel* ResourceParcel::clone() { return new ResourceParcel(*this); }

void ResourceParcel::add(const ResourceType& resource, const size_t amount)
{
    try {
        resources.at(resource) += amount;
    } catch (const std::out_of_range& e) {
        resources[resource] = amount;
    }
}

void ResourceParcel::remove(const ResourceType& resource, const size_t amount)
{
    // Can throw std::out_of_range
    resources.at(resource) -= amount;
}

bool ResourceParcel::empty() const { return resources.empty(); }

ResourceSale::ResourceSale(ResourceParcel* resources, Elektro price)
    : Sale(resources, price)
{
}

ResourceSale::ResourceSale(const ResourceSale& source) : Sale(source) {}

ResourceSale::ResourceSale(ResourceSale&& source) : Sale(std::move(source)) {}

ResourceSale::~ResourceSale() {}

ResourceParcel* ResourceSale::claim(Buyer* buyer)
{
    return static_cast<ResourceParcel*>(Sale::claim(buyer));
}

ResourceSale& ResourceSale::operator=(const ResourceSale& source)
{
    Sale::operator=(source);
    return *this;
}

Market::~Market() {}

PowerplantMarket::PowerplantMarket() {}

PowerplantMarket::PowerplantMarket(Deck& deck) : deck(deck)
{
    for (size_t i = 0; i < 8; i++) {
        market[i] = static_cast<PowerplantCard*>(
            this->deck.draw()); // If the cards on top are not PowerplantCards,
                                // then somthing went wrong
    }

    powerplantsOnMarket = 8;
    sortMarket();
}

PowerplantMarket::PowerplantMarket(const PowerplantMarket& source)
    : deck(source.deck), step3Drawn(source.step3Drawn),
      powerplantsOnMarket(source.powerplantsOnMarket)
{
    for (size_t i = 0; i < powerplantsOnMarket; i++) {
        market[i] = new PowerplantCard(*source.market[i]);
    }

    if (source.sale) {
        sale = new PowerplantSale(*source.sale);
    }
}

PowerplantMarket::PowerplantMarket(PowerplantMarket&& source)
    : deck(std::move(source.deck)), step3Drawn(std::move(source.step3Drawn)),
      market(std::move(source.market)),
      powerplantsOnMarket(std::move(source.powerplantsOnMarket)),
      sale(std::move(source.sale))
{
}

PowerplantMarket::~PowerplantMarket()
{
    for (size_t i = 0; i < powerplantsOnMarket; i++) {
        delete market[i];
        market[i] = nullptr;
    }
}

void PowerplantMarket::ringup(PowerplantSale* sale) { this->sale = sale; }

void PowerplantMarket::checkout(Buyer* buyer)
{
    buyer->buy(sale);
    delete sale;
    sale = nullptr;
}

Amount PowerplantMarket::checkStock() const { return powerplantsOnMarket; }

PowerplantCard* PowerplantMarket::select(Index selection)
{
    if (selection >= powerplantsOnMarket)
        return nullptr;

    PowerplantCard* powerplant = market[selection];
    if (selection == --powerplantsOnMarket) {
        market[selection] = nullptr;
    } else {
        market[selection]           = market[powerplantsOnMarket];
        market[powerplantsOnMarket] = nullptr;
        if (!deck.empty()) {
            Card* card = deck.draw();
            if (card->isStep3Card()) {
                step3Drawn = true;
                card       = deck.draw(); // Step 3 card should not be on bottom
            }
            PowerplantCard* powerplant    = static_cast<PowerplantCard*>(card);
            market[powerplantsOnMarket++] = powerplant;
        }
    }

    sortMarket();

    return powerplant;
}

const PowerplantCard* PowerplantMarket::preview(Index selection) const
{
    if (selection >= powerplantsOnMarket)
        return nullptr;

    return market[selection];
}

void PowerplantMarket::sortMarket()
{
    std::sort(market.begin(), market.end(),
              [](PowerplantCard* first, PowerplantCard* second) {
                  return *first < *second;
              });
}

PowerplantMarket& PowerplantMarket::operator=(PowerplantMarket source)
{
    swap(*this, source);
    return *this;
}

ResourceMarket::ResourceMarket()
{
    for (const ResourceType& resource : ResourceTypes::enumerate()) {
        Amount supply =
            ResourceTypes::getProperty(resource, Property::initialSupply);
        Limit poolSize =
            ResourceTypes::getProperty(resource, Property::poolSize);
        Value volatility =
            ResourceTypes::getProperty(resource, Property::volatility);

        Commodity commodity(resource, supply, poolSize, volatility);
        commodities.insert(std::make_pair(resource, commodity));
    }
}

ResourceMarket::ResourceMarket(const ResourceMarket& source)
    : commodities(source.commodities)
{
    if (source.sale) {
        sale = new ResourceSale(*source.sale);
    }
}

ResourceMarket::ResourceMarket(ResourceMarket&& source)
    : commodities(std::move(source.commodities)), sale(std::move(source.sale))
{
}

ResourceMarket::~ResourceMarket() {}

void ResourceMarket::peruse(Cart* cart) { this->cart = cart; }

void ResourceMarket::ringup(ResourceSale* sale) { this->sale = sale; }

void ResourceMarket::checkout(Buyer* buyer)
{
    buyer->buy(sale);
    delete sale;
    sale = nullptr;
}

void ResourceMarket::take(const ResourceType& resource, size_t amount)
{
    // Can throw std::runtime_error
    commodities[resource].take(amount);
    cart->add(commodities[resource], amount);
}

void ResourceMarket::putBack(const ResourceType& resource, size_t amount)
{
    cart->remove(commodities[resource], amount);
    commodities[resource].putBack(amount);
}

void ResourceMarket::stock(const ResourceType& resource, size_t amount)
{
    commodities[resource].stock(amount);
}

void ResourceMarket::recycle(const ResourceType& resource, size_t amount)
{
    commodities[resource].recycle(amount);
}

ResourceMarket& ResourceMarket::operator=(ResourceMarket source)
{
    swap(*this, source);
    return *this;
}

void swap(Sale& first, Sale& second)
{
    using std::swap;

    swap(first.item, second.item);
    swap(first.price, second.price);
}

void swap(PowerplantMarket& first, PowerplantMarket& second)
{
    using std::swap;

    swap(first.deck, second.deck);
    swap(first.step3Drawn, second.step3Drawn);
    swap(first.market, second.market);
    swap(first.powerplantsOnMarket, second.powerplantsOnMarket);
    swap(first.sale, second.sale);
}

std::ostream& operator<<(std::ostream& out, const PowerplantMarket& rhs)
{
    size_t actualPlants = std::min((size_t)4, rhs.powerplantsOnMarket);
    std::vector<std::string> labels;

    ArrangeCards actualLineup;
    actualLineup(rhs.market.cbegin(), rhs.market.cbegin() + actualPlants);
    actualLineup.border(true);

    for (size_t i = 1; i <= actualPlants; ++i) {
        labels.push_back(std::to_string(i));
    }

    actualLineup.labels(labels);

    ArrangeCards futureLineup;
    futureLineup(rhs.market.cbegin() + actualPlants,
                 rhs.market.cbegin() + rhs.powerplantsOnMarket);
    futureLineup.border(true);

    labels.clear();

    if (actualPlants == 4) {
        for (size_t i = 0; i < rhs.powerplantsOnMarket - 4; ++i) {
            std::string prefix = std::to_string(i + 5);
            labels.push_back(prefix + "/NA");
        }
    }

    futureLineup.labels(labels);

    out << actualLineup << "\n\n" << futureLineup;
    return out;
}

void swap(ResourceMarket& first, ResourceMarket& second)
{
    using std::swap;

    swap(first.commodities, second.commodities);
    swap(first.sale, second.sale);
}

std::ostream& operator<<(std::ostream& out, const ResourceMarket& rhs)
{
    out << "Cart: " << *rhs.cart << "\n\n";
    for (const ResourceType& resource : ResourceTypes::enumerate()) {
        out << rhs.commodities.at(resource);
        if (resource != *(ResourceTypes::end() - 1)) {
            out << "\n";
        }
    }

    return out;
}
