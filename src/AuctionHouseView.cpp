#include "AuctionHouseView.h"
#include "AuctionHouse.h"
#include "Card.h"
#include "Definition.h"
#include "InputScene.h"
#include "InputView.h"
#include "Observer.h"
#include "Player.h"
#include "Subject.h"
#include "util/util.h"

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

AuctionHouseView::InitialScene::InitialScene(AuctionHouseView* view)
    : view(view)
{
}

AuctionHouseView::InitialScene::~InitialScene() {}

void AuctionHouseView::InitialScene::show()
{
    clear_term();

    std::cout << "Player: " << colorToString.at(view->color) << "     "
              << "Elektro: " << view->elektro;

    std::cout << "\n\n"
              << "Currently Auctioning:\n\n"
              << *view->auctioning << "\n\n"
              << "Current bid: " << view->bid;
}

bool AuctionHouseView::InitialScene::input(Validator* validator)
{
    if (view->auctionHouse->getCurrentBid() >= view->elektro) {
        std::cout << "\n\n"
                  << "You do not have enough elektro to continue bidding\n"
                  << "Press ENTER to contine";

        std::cin.ignore();
        delete this;
        return false;
    }

    std::string input;

    std::cout << "\n\n"
              << "Would you like to bid on this powerplant ([y]/n): ";

    while (!getline(std::cin, input)) {
        std::cin.clear();
    }

    try {
        std::string sanitized = validator->validate(input);

        if (sanitized == "y") {
            view->scene = new BidScene(view);
            delete this;
            return true;
        } else if (sanitized == "n") {
            delete this;
            return false;
        }

    } catch (const std::runtime_error& e) {
        std::cerr << "\n" << e.what() << "\n";
        std::cout << "Press Enter to continue";
        std::cin.ignore();
    }

    return true;
}

AuctionHouseView::BidScene::BidScene(AuctionHouseView* view) : view(view) {}

AuctionHouseView::BidScene::~BidScene() {}

void AuctionHouseView::BidScene::show()
{
    clear_term();

    std::cout << "Player: " << colorToString.at(view->color) << "     "
              << "Elektro: " << view->elektro;

    std::cout << "\n\n"
              << "Currently Auctioning:\n\n"
              << *view->auctioning << "\n\n"
              << "Current bid: " << view->bid;
}

bool AuctionHouseView::BidScene::input(Validator* validator)
{
    std::string input;

    size_t min;
    std::string note   = "";
    std::string cancel = "";

    bool mustBid = view->auctionHouse->isOpeningBid();
    if (mustBid) {
        min  = view->auctioning->getNumber();
        note = "You started this auction so you must make a bid\n";
    } else {
        min    = view->auctionHouse->getCurrentBid() + 1;
        cancel = ", c to cancel";
    }

    std::cout << "\n\n"
              << "How much do you want to bid?\n"
              << note << "enter amount (min [" << min << "]" << cancel << "): ";

    while (!getline(std::cin, input)) {
        std::cin.ignore();
    }

    try {
        std::string sanitized = validator->validate(input);

        if (sanitized == "c") {
            view->scene = new InitialScene(view);
            delete this;
            return true;
        } else {
            delete this;
            return false;
        }

    } catch (std::runtime_error& e) {
        std::cerr << "\n" << e.what() << "\n";
        std::cout << "Press ENTER to continue";
        std::cin.ignore();
    }

    return true;
}

AuctionHouseView::AuctionHouseView(AuctionHouse* auctionHouse, Roster* roster)
    : auctionHouse(auctionHouse), roster(roster)
{
    auctionHouse->attach(this);
    roster->attach(this);
}

AuctionHouseView::~AuctionHouseView() { delete scene; }

void AuctionHouseView::show() { scene->show(); }

bool AuctionHouseView::input(Validator* validator)
{
    bool proceed = scene->input(validator);
    if (!proceed)
        scene = nullptr;
    return proceed;
}

void AuctionHouseView::update()
{
    const Player* currentPlayer = roster->current();
    color                       = currentPlayer->getColor();
    elektro                     = currentPlayer->getElektro();

    std::stringstream currentAuction;

    auctioning = auctionHouse->getAuctioning();

    delete scene;
    if (auctionHouse->isOpeningBid()) {
        bid   = "No bids placed";
        scene = new BidScene(this);
    } else {
        bid   = std::to_string(auctionHouse->getCurrentBid());
        scene = new InitialScene(this);
    }
}

void AuctionHouseView::announceWinner(const PowerplantCard* powerplant,
                                      const Player* winner) const
{
    clear_term();

    std::string price;
    if (auctionHouse->isOpeningBid()) {
        price = std::to_string(powerplant->getNumber());
    } else {
        price = bid;
    }

    std::cout << "Congratulations Player: "
              << colorToString.at(winner->getColor()) << "\n\n"
              << "You won:"
              << "\n\n"
              << *powerplant << "\n\n"
              << "for " << price << " Elektro"
              << "\n\n"
              << "Press ENTER to continue";

    std::cin.ignore();
}
