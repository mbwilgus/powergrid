#ifndef __RESOURCE_MARKET_VIEW_H__
#define __RESOURCE_MARKET_VIEW_H__

#include "InputScene.h"
#include "InputView.h"
#include "Market.h"
#include "ResourceType.h"
#include "Roster.h"
#include "Validator.h"

#include <unordered_map>

class ResourceMarketView : public InputView
{
  private:
    class InitialScene : public InputScene
    {
      private:
        ResourceMarketView* view;

      public:
        InitialScene(ResourceMarketView* view);

        virtual ~InitialScene();

        virtual void show() override;
        virtual bool input(Validator* validator) override;
    };

    const ResourceMarket* market;
    const Roster* roster;

    Color color;
    size_t elektro;

    std::unordered_map<ResourceType, size_t> quotas;

    InputScene* scene = nullptr;

  public:
    ResourceMarketView(ResourceMarket* market, Roster* roster);

    virtual ~ResourceMarketView();

    virtual void show() override;
    virtual bool input(Validator* validator) override;

    virtual void update() override;
};

#endif
