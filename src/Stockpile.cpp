#include "Stockpile.h"
#include "Definition.h"
#include "ResourceType.h"

#include <iostream>

Stockpile::Stockpile()
{
    for (const ResourceType& resource : ResourceTypes::enumerate()) {
        stored[resource] = std::make_pair<Amount, Limit>(0, 0);
        relatedResources[resource];
    }
}

Stockpile::Stockpile(const Stockpile& source)
    : stored(source.stored), relatedResources(source.relatedResources)
{
}

Stockpile::Stockpile(Stockpile&& source)
    : stored(std::move(source.stored)),
      relatedResources(std::move(source.relatedResources))
{
}

Limit Stockpile::getStorageLimit(const ResourceType& resource) const
{
    return stored.at(resource).second;
}

Amount Stockpile::getAmountAvailable(const ResourceType& resource) const
{
    return stored.at(resource).first;
}

Amount Stockpile::getStorageSpace(const ResourceType& resource) const
{
    return stored.at(resource).second - stored.at(resource).first;
}

ResourceQuota Stockpile::getResourceQuotas() const
{
    ResourceQuota quotas;
    for (const auto& pair : stored) {
        quotas[pair.first] = pair.second.second - pair.second.first;
    }
    return quotas;
}

bool Stockpile::canStore(const ResourceType& resource) const
{
    return stored.at(resource).second - stored.at(resource).first > 0;
}

void Stockpile::increaseStorageLimit(const ResourceType& resource,
                                     const Amount amount)
{
    stored[resource].second += amount;
}

void Stockpile::decreaseStorageLimit(const ResourceType& resource,
                                     const Amount amount)
{
    stored[resource].second -= amount;
}

void Stockpile::store(const ResourceType& resource, const Amount amount)
{
    stored[resource].first += amount;
}

void Stockpile::withdraw(const ResourceType& resource, const Amount amount)
{
    stored[resource].first -= amount;
}

const ResourceSet&
Stockpile::getRelatedResources(const ResourceType& resource) const
{
    return relatedResources.at(resource);
}

void Stockpile::relateResources(const ResourceType& first,
                                const ResourceType& second)
{
    relatedResources[first].insert(second);
    relatedResources[second].insert(first);
}

Stockpile& Stockpile::operator=(Stockpile source)
{
    swap(*this, source);
    return *this;
}

std::ostream& operator<<(std::ostream& out, const Stockpile& stockpile)
{
    for (const auto& pair : stockpile.stored) {
        char token = pair.first.name()[0];
        out << token << ": " << pair.second.first << "/" << pair.second.second
            << "\t";
    }
    return out;
}

void swap(Stockpile& first, Stockpile& second)
{
    using std::swap;

    swap(first.stored, second.stored);
    swap(first.relatedResources, second.relatedResources);
}
