#ifndef __SUBJECT_H__
#define __SUBJECT_H__

#include <vector>

class Observer;

class Subject
{
  private:
    std::vector<Observer*> observers;

  public:
    void attach(Observer* observer);
    void detach(Observer* observer);
    void notify();
};

#endif
