#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "Buyer.h"
#include "Card.h"
#include "City.h"
#include "Definition.h"
#include "Map.h"
#include "Market.h"
#include "PropertyManager.h"
#include "ResourceType.h"
#include "Stockpile.h"
#include "Subject.h"

#include <array>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility> // For swap

#define _P_UNUSED_ __attribute__((unused))

class NullPlayer;

class Player : public Buyer
{
  private:
    Color color;
    Elektro elektro;

    PropertyManager powerplants;
    Stockpile resources;
    Map network;

  public:
    Player();
    Player(Color color, Elektro elektro);
    Player(const Player& source);
    Player(Player&& source);

    ~Player();

    Color getColor() const;
    Elektro getElektro() const;

    void shop(Market* market);

    virtual void buy(PowerplantSale* sale) override;

    virtual void buy(ResourceSale* sale) override;

    virtual void spend(Elektro amount) override;

    Amount numberOfPlantsOwned() const;

    void discard(Index index);

    const PropertyManager* getPropertyManager() const;

    Limit getStorageLimit(const ResourceType& resource) const;
    Amount getAmountAvailable(const ResourceType& resource) const;

    ResourceQuota getResourceQuotas() const;

    bool canStore(const ResourceType& reosurce) const;
    const ResourceSet& getRelatedResources(const ResourceType& resource) const;

    void build(City& city);

    void power();

    Player& operator=(Player source);

    // Typically defined
    bool operator==(const Player& rhs) const;
    bool operator<(const Player& rhs) const;

    // Defined by using == and <
    bool operator!=(const Player& rhs) const;
    bool operator>(const Player& rhs) const;

    bool operator==(_P_UNUSED_ const NullPlayer& rhs) const;
    bool operator<(_P_UNUSED_ const NullPlayer& rhs) const;
    bool operator!=(_P_UNUSED_ const NullPlayer& rhs) const;
    bool operator>(_P_UNUSED_ const NullPlayer& rhs) const;

    friend void swap(Player& first, Player& second);
};

class NullPlayer : public Player
{
  public:
    bool operator==(_P_UNUSED_ const NullPlayer& rhs) const;
    bool operator<(_P_UNUSED_ const NullPlayer& rhs) const;
    bool operator!=(_P_UNUSED_ const NullPlayer& rhs) const;
    bool operator>(_P_UNUSED_ const NullPlayer& rhs) const;

    bool operator==(const Player& rhs) const;
    bool operator<(const Player& rhs) const;
    bool operator!=(const Player& rhs) const;
    bool operator>(const Player& rhs) const;
};

#endif
