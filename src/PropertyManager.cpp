#include "PropertyManager.h"
#include "Card.h"
#include "Controller.h"
#include "Definition.h"

#include <array>
#include <type_traits>
#include <utility>

PropertyManager::PropertyManager() {}

PropertyManager::PropertyManager(const PropertyManager& source)
    : owned(source.owned)
{
    for (size_t i = 0; i < source.owned; i++) {
        PowerplantCard* copy = new PowerplantCard(*source.powerplants[i]);
        powerplants[i]       = copy;
    }
}

PropertyManager::PropertyManager(PropertyManager&& source)
    : powerplants(std::move(source.powerplants)), owned(std::move(source.owned))
{
}

PropertyManager::~PropertyManager()
{
    for (size_t i = 0; i < owned; i++) {
        delete powerplants[i];
        powerplants[i] = nullptr;
    }
}

Amount PropertyManager::numberOfPlantsOwned() const { return owned; }

Value PropertyManager::highestValuedPlant() const
{
    if (owned > 0) {
        return powerplants[0]->getNumber();
    } else {
        return 0;
    }
}

const PowerplantCard* PropertyManager::getStoring() const { return storing; }

void PropertyManager::claimProperty(PowerplantCard* powerplant)
{
    if (owned == 3) {
        storing = powerplant;
        notify();
        GameController::getInstance()->triggerNextEvent();
        storing = nullptr;
    }

    powerplants[owned++] = powerplant;

    // Selection sort the array. Is O(1) because n = O(1)
    for (size_t i = 0; i < owned - 1; i++) {
        for (size_t j = i + 1; j < owned; j++) {
            if (*powerplants[i] < *powerplants[j]) {
                PowerplantCard* temp = powerplants[i];
                powerplants[i]       = powerplants[j];
                powerplants[j]       = temp;
            }
        }
    }
}

PowerplantCard* PropertyManager::relinquishProperty(const Index index)
{
    PowerplantCard* powerplant = powerplants[index];
    if (owned > index + 1) {
        for (size_t i = index + 1; i < owned; i++) {
            powerplants[i - 1] = powerplants[i];
        }
    }
    powerplants[--owned] = nullptr;

    return powerplant;
}

PropertyManager::const_iterator PropertyManager::begin() const
{
    return powerplants.cbegin();
}

PropertyManager::const_iterator PropertyManager::end() const
{
    return powerplants.cbegin() + owned;
}

PropertyManager::const_reverse_iterator PropertyManager::rbegin() const
{
    return powerplants.crbegin() + 3 - owned;
}

PropertyManager::const_reverse_iterator PropertyManager::rend() const
{
    return powerplants.crend();
}

PropertyManager& PropertyManager::operator=(PropertyManager source)
{
    swap(*this, source);
    return *this;
}

void swap(PropertyManager& first, PropertyManager& second)
{
    using std::swap;

    swap(first.powerplants, second.powerplants);
    swap(first.owned, second.owned);
}
