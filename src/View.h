#ifndef __VIEW_H__
#define __VIEW_H__

#include "Observer.h"
#include "Presentable.h"
#include "Subject.h"

#include <string>

class View : public Presentable, public Observer
{
  public:
    virtual ~View();

    virtual void show() override = 0;

    virtual void update() override = 0;
};

#endif
