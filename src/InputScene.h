#ifndef __INPUT_SCENE_H__
#define __INPUT_SCENE_H__

#include "InputEvent.h"
#include "Scene.h"
#include "Validator.h"

class InputScene : public Scene, public InputEvent
{
  public:
    virtual ~InputScene();

    virtual void show() override = 0;

    virtual bool input(Validator* validator) override = 0;
};

#endif
