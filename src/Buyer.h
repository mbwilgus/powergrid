#ifndef __BUYER_H_
#define __BUYER_H_

#include "Definition.h"
#include "Market.h"

class Buyer
{
  public:
    virtual ~Buyer();
    virtual void buy(PowerplantSale* sale) = 0;
    virtual void buy(ResourceSale* sale)   = 0;

    virtual void spend(Elektro amount) = 0;
};

#endif
