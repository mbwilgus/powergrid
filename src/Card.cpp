#include "Card.h"
#include "ResourceType.h"

#include <algorithm>
#include <array>
#include <ios>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

Card::~Card() {}

Step3Card::Step3Card() {}

Step3Card::~Step3Card() {}

bool Step3Card::isStep3Card() const { return true; }

PowerplantCard::PowerplantCard() {}

PowerplantCard::PowerplantCard(int number, int needs, int powers)
    : number(number), needs(needs), powers(powers)
{
}

PowerplantCard::PowerplantCard(const PowerplantCard& source)
    : number(source.number), needs(source.needs), powers(source.powers),
      types(source.types)
{
}

PowerplantCard::PowerplantCard(PowerplantCard&& source)
    : number(std::move(source.number)), needs(std::move(source.needs)),
      powers(std::move(source.powers)), types(std::move(source.types))
{
}

PowerplantCard::~PowerplantCard() {}

bool PowerplantCard::isStep3Card() const { return false; }

PowerplantCard* PowerplantCard::clone() { return new PowerplantCard(*this); }

const std::vector<ResourceType>& PowerplantCard::usableResources() const
{
    return types;
}

PowerplantCard::const_iterator PowerplantCard::begin() const
{
    return types.begin();
}

PowerplantCard::const_iterator PowerplantCard::end() const
{
    return types.end();
}

int PowerplantCard::getNumber() const { return number; }

int PowerplantCard::getNeeded() const { return needs; }

int PowerplantCard::getStorageSpace() const { return 2 * needs; }

int PowerplantCard::getPowerable() const { return powers; }

PowerplantCard& PowerplantCard::operator=(PowerplantCard source)
{
    swap(*this, source);

    return *this;
}

bool PowerplantCard::operator<(const PowerplantCard& rhs) const
{
    return number < rhs.number;
}

PowerplantBuilder::PowerplantBuilder() {}

PowerplantBuilder::PowerplantBuilder(int number, int needs, int powers)
{
    powerplant = new PowerplantCard(number, needs, powers);
}

PowerplantBuilder::~PowerplantBuilder() {}

void PowerplantBuilder::fueledBy(const ResourceType& resource)
{
    powerplant->types.push_back(resource);
}

PowerplantCard* PowerplantBuilder::getPowerplant()
{
    PowerplantCard* _powerplant = powerplant;
    powerplant                  = nullptr;
    return _powerplant;
}

std::ostream& operator<<(std::ostream& out, const PowerplantCard& card)
{
    out << "\u2554\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550"
           "\u2550\u2550\u2557";
    out << std::endl;
    out << "\u2551"
        << "\u26EE : ";
    out.width(8);
    out << std::right << card.getNumber();
    out << "\u2551" << std::endl;
    std::string needed;
    if (card.getNeeded() == 0) {
        needed = "ecological";
    } else {
        needed = std::to_string(card.getNeeded());
    }
    out << "\u2551"
        << "\u2301 : ";
    out.width(8);
    out << std::right << needed;
    out << "\u2551" << std::endl;
    out << "\u2551"
        << "\u2302 : ";
    out.width(8);
    out << std::right << card.getPowerable();
    out << "\u2551" << std::endl;
    out << "\u2551"
        << "\u26fd\ufe0e : ";
    std::string resources;
    if (card.getNeeded() > 0) {
        for (auto& resource : card.types) {
            resources += resource.name();
            if (resource.name() != (card.types.end() - 1)->name()) {
                resources += ' ';
            }
        }
        out.width(8);
        out << std::left << resources;
        out << "\u2551";
    }
    out << std::endl;
    out << "\u255a\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550"
           "\u2550\u2550\u255d";
    return out;
}

void swap(PowerplantCard& first, PowerplantCard& second)
{
    using std::swap;

    swap(first.number, second.number);
    swap(first.needs, second.needs);
    swap(first.powers, second.powers);
    swap(first.types, second.types);
}
