#ifndef __CARD_H__
#define __CARD_H__

#include "Buyable.h"
#include "ResourceType.h"

#include <iostream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

class Card
{
  public:
    virtual ~Card();
    virtual bool isStep3Card() const = 0;
};

class Step3Card : public Card
{
  public:
    Step3Card();
    virtual ~Step3Card();
    virtual bool isStep3Card() const override;
};

class PowerplantCard : public Card, public Buyable
{
  private:
    int number;
    int needs;
    int powers;
    std::vector<ResourceType> types;

  public:
    PowerplantCard();
    PowerplantCard(int number, int needs, int powers);
    PowerplantCard(const PowerplantCard& source);
    PowerplantCard(PowerplantCard&& source);
    virtual ~PowerplantCard();

    virtual bool isStep3Card() const override;

    virtual PowerplantCard* clone() override;

    const std::vector<ResourceType>& usableResources() const;

    typedef std::vector<ResourceType>::const_iterator const_iterator;
    const_iterator begin() const;
    const_iterator end() const;

    int getNumber() const;
    int getNeeded() const;
    int getStorageSpace() const;
    int getPowerable() const;

    PowerplantCard& operator=(PowerplantCard source);
    bool operator<(const PowerplantCard& rhs) const;

    friend std::ostream& operator<<(std::ostream& out,
                                    const PowerplantCard& card);
    friend void swap(PowerplantCard& first, PowerplantCard& second);

    friend class PowerplantBuilder;
};

class PowerplantBuilder
{
  private:
    PowerplantCard* powerplant;

  public:
    PowerplantBuilder();
    PowerplantBuilder(int number, int needs, int powers);
    ~PowerplantBuilder();

    void fueledBy(const ResourceType& resource);
    PowerplantCard* getPowerplant();
};

std::string arrangeCardsInRow(std::vector<PowerplantCard>& powerplants);

#endif
