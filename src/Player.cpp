#include "Player.h"
#include "Buyer.h"
#include "Card.h"
#include "Controller.h"
#include "Definition.h"
#include "Market.h"
#include "PropertyManager.h"
#include "ResourceType.h"

#include <algorithm> // For copy
#include <array>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility> // For swap, move
#include <vector>

#define _P_UNUSED_ __attribute__((unused))

Player::Player() {}

Player::Player(Color color, Elektro elektro) : color(color), elektro(elektro) {}

Player::Player(const Player& source)
    : color(source.color), elektro(source.elektro),
      powerplants(source.powerplants), resources(source.resources)
{
}

Player::Player(Player&& source)
    : color(std::move(source.color)), elektro(std::move(source.elektro)),
      powerplants(std::move(source.powerplants)),
      resources(std::move(source.resources))
{
}

Player::~Player() {}

Color Player::getColor() const { return color; }

Elektro Player::getElektro() const { return elektro; }

void Player::shop(Market* market) {}

void Player::buy(PowerplantSale* sale)
{
    PowerplantCard* powerplant = sale->claim(this);

    powerplants.claimProperty(powerplant);

    const std::vector<ResourceType>& types = powerplant->usableResources();
    for (const ResourceType& resource : types) {
        resources.increaseStorageLimit(resource, powerplant->getStorageSpace());
    }

    std::vector<std::pair<ResourceType, ResourceType>> pairs =
        ResourceTypes::getResourcePairs(types);
    for (const auto& pair : pairs) {
        resources.relateResources(pair.first, pair.second);
    }
}

void Player::buy(ResourceSale* sale)
{
    ResourceParcel* unstored = sale->claim(this);

    /* for (const auto& pair : *unstored) { */
    /*     resources.store(pair.first, pair.second); */
    /* } */

    delete unstored;
}

void Player::spend(Elektro amount) { elektro -= amount; }

Amount Player::numberOfPlantsOwned() const
{
    return powerplants.numberOfPlantsOwned();
}

void Player::discard(Index index)
{
    PowerplantCard* discarding = powerplants.relinquishProperty(index);
    delete discarding;
    discarding = nullptr;
}

const PropertyManager* Player::getPropertyManager() const
{
    return &powerplants;
}

Limit Player::getStorageLimit(const ResourceType& resource) const
{
    return resources.getStorageLimit(resource);
}

Amount Player::getAmountAvailable(const ResourceType& resource) const
{
    return resources.getAmountAvailable(resource);
}

ResourceQuota Player::getResourceQuotas() const
{
    return resources.getResourceQuotas();
}

bool Player::canStore(const ResourceType& resource) const
{
    return resources.canStore(resource);
}

const ResourceSet&
Player::getRelatedResources(const ResourceType& resource) const
{
    return resources.getRelatedResources(resource);
}

Player& Player::operator=(Player source)
{
    swap(*this, source);
    return *this;
}

bool Player::operator==(const Player& rhs) const { return color == rhs.color; }
bool Player::operator<(const Player& rhs) const
{
    if (network.getSize() == rhs.network.getSize()) {
        return powerplants.highestValuedPlant() <
               rhs.powerplants.highestValuedPlant();
    } else {
        return network.getSize() < rhs.network.getSize();
    }
}

bool Player::operator!=(const Player& rhs) const { return !operator==(rhs); }

bool Player::operator>(const Player& rhs) const { return rhs < *this; }

bool Player::operator==(_P_UNUSED_ const NullPlayer& rhs) const { return false; }

bool Player::operator<(_P_UNUSED_ const NullPlayer& rhs) const { return false; }

bool Player::operator!=(_P_UNUSED_ const NullPlayer& rhs) const { return true; }

bool Player::operator>(_P_UNUSED_ const NullPlayer& rhs) const { return true; }

bool NullPlayer::operator==(_P_UNUSED_ const NullPlayer& rhs) const { return true; }

bool NullPlayer::operator<(_P_UNUSED_ const NullPlayer& rhs) const { return false; }

bool NullPlayer::operator!=(_P_UNUSED_ const NullPlayer& rhs) const { return false; }

bool NullPlayer::operator>(_P_UNUSED_ const NullPlayer& rhs) const { return false; }

bool NullPlayer::operator==(_P_UNUSED_ const Player& rhs) const { return false; }

bool NullPlayer::operator<(_P_UNUSED_ const Player& rhs) const { return true; }

bool NullPlayer::operator!=(_P_UNUSED_ const Player& rhs) const { return true; }

bool NullPlayer::operator>(_P_UNUSED_ const Player& rhs) const { return false; }

void swap(Player& first, Player& second)
{
    using std::swap;

    swap(first.color, second.color);
    swap(first.elektro, second.elektro);
    swap(first.powerplants, second.powerplants);
    swap(first.resources, second.resources);
}
