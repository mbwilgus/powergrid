#ifndef __INPUT_EVENT_H__
#define __INPUT_EVENT_H__

#include "Validator.h"

class InputEvent
{
  public:
    virtual ~InputEvent();

    virtual bool input(Validator* validator) = 0;
};

#endif
