#include "AuctionHouse.h"
#include "Card.h"
#include "Definition.h"
#include "Player.h"
#include "Roster.h"

#include <utility>

AuctionHouse::AuctionHouse() {}

AuctionHouse::AuctionHouse(const AuctionHouse& source)
    : roster(source.roster), auctioning(source.auctioning),
      currentBid(source.currentBid)
{
}

void AuctionHouse::startAuction(Roster* roster, PowerplantCard* powerplant)
{
    roster->save();
    roster->toggleAuctionMode();
    this->roster = roster;

    auctioning = powerplant;
    currentBid = powerplant->getNumber();

    openingBid = true;

    notify();
}

AuctionResult AuctionHouse::declareWinner()
{
    PowerplantSale* sale = new PowerplantSale(auctioning, currentBid);
    AuctionResult result = std::make_pair(sale, roster->current());

    currentBid = 0;
    auctioning = nullptr;

    roster->toggleAuctionMode();
    roster->restore();
    roster = nullptr;

    return result;
}

const PowerplantCard* AuctionHouse::getAuctioning() const { return auctioning; }

Amount AuctionHouse::getCurrentBid() const { return currentBid; }

bool AuctionHouse::isOpeningBid() const { return openingBid; }

void AuctionHouse::placeBid(Amount bid)
{
    Player* current = roster->current();
    if (bid > 0) {

        if (openingBid) {
            openingBid = false;
        }

        currentBid = bid;

        notify();
    } else {
        roster->bench(current);
    }
}
