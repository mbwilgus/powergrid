#ifndef __CART_H__
#define __CART_H__

#include "Commodity.h"
#include "Definition.h"
#include "ResourceType.h"

class ResourceParcel;
class ResourceSale;

class Cart
{
  private:
    ResourceParcel* cart;
    Elektro cost = 0;

  public:
    Cart() noexcept;

    void add(Commodity& commodity, size_t amount) noexcept;
    void remove(Commodity& commodity, size_t amount);

    ResourceSale* makeSale() noexcept;

    friend std::ostream& operator<<(std::ostream& out,
                                    const Cart& rhs) noexcept;
};

#endif
