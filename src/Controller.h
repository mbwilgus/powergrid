#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include "Command.h"
#include "Presentable.h"

#include <deque>
#include <unordered_map>

class GameController
{
  private:
    static GameController* instance;
    std::deque<Command*> commandQueue;
    std::deque<Presentable*> eventQueue;

  protected:
    GameController();
    virtual ~GameController();

  public:
    static GameController* getInstance();
    static void deleteInstance();

    void invoke(Command* command);
    void queueEvent(Presentable* event);
    void triggerNextEvent();
};

#endif
