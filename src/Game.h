#ifndef __GAME_H__
#define __GAME_H__

#include "AuctionHouse.h"
#include "AuctionHouseView.h"
#include "Definition.h"
#include "Map.h"
#include "Market.h"
#include "Player.h"
#include "PowerplantMarketView.h"
#include "ResourceMarketView.h"
#include "Roster.h"
#include "Subject.h"
#include "View.h"

#include <string>
#include <vector>

class Game : public Subject
{
  private:
    std::vector<Player*> players;
    Roster* roster;
    /* Map map; */
    PowerplantMarket* escrow;
    AuctionHouse* auctionHouse;
    ResourceMarket* refinery;

    PowerplantMarketView* escrowView;
    AuctionHouseView* auctionHouseView;
    ResourceMarketView* refineryView;

    Player* currentPlayer;

    PowerplantCard* onPowerplantSelected();
    void onBidMade();
    void onPurchaseResources();

  public:
    Game();
    Game(std::vector<Player*>& players,
         /* Map& map, */ PowerplantMarket* escrow, AuctionHouse* auctionHouse,
         ResourceMarket* refinery);

    ~Game();

    void phase2();
    void phase3();

    // For testing
    friend class MockGame;
};

#endif
