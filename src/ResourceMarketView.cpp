#include "ResourceMarketView.h"
#include "Definition.h"
#include "Market.h"
#include "Player.h"
#include "Roster.h"
#include "Validator.h"
#include "util/util.h"

#include <iostream>

ResourceMarketView::InitialScene::InitialScene(ResourceMarketView* view)
    : view(view)
{
}

ResourceMarketView::InitialScene::~InitialScene() {}

void ResourceMarketView::InitialScene::show()
{
    clear_term();

    std::cout << "Player: " << colorToString.at(view->color) << "     "
              << "Elektro: " << view->elektro << "\n";

    for (const ResourceType& resource : ResourceTypes::enumerate()) {
        std::cout << resource.name()[0] << ": "
                  << view->quotas[resource] << "     ";
    }

    std::cout << "\n\n" << *view->market;
}

bool ResourceMarketView::InitialScene::input(Validator* validator)
{
    std::string input;

    std::cout << "\n\n"
              << "Would you like to buy resources ([y]/n): ";

    while (!getline(std::cin, input)) {
        std::cin.clear();
    }

    try {
        std::string sanitized = validator->validate(input);

        if (sanitized == "y") {
            delete this;
            return false;
        } else if (sanitized == "n") {
            delete this;
            return false;
        }

    } catch (const ValidationError& e) {
        std::cerr << "\n" << e.what() << "\n";
        std::cout << "Press ENTER to continue";
        std::cin.ignore();
    }

    return true;
}

ResourceMarketView::ResourceMarketView(ResourceMarket* market, Roster* roster)
    : market(market), roster(roster)
{
    market->attach(this);
    roster->attach(this);
}

ResourceMarketView::~ResourceMarketView() { delete scene; }

void ResourceMarketView::show() { scene->show(); }

bool ResourceMarketView::input(Validator* validator)
{
    bool proceed = scene->input(validator);
    if (!proceed) {
        scene = nullptr;
    }
    return proceed;
}

void ResourceMarketView::update()
{
    const Player* currentPlayer = roster->current();
    color = currentPlayer->getColor();
    elektro = currentPlayer->getElektro();
    quotas = currentPlayer->getResourceQuotas();

    delete scene;
    scene = new InitialScene(this);
}
