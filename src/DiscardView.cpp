#include "DiscardView.h"
#include "ArrangeCards.h"
#include "Card.h"
#include "Command.h"
#include "Controller.h"
#include "Definition.h"
#include "Game.h"
#include "Observer.h"
#include "Player.h"
#include "PropertyManager.h"
#include "ResourceType.h"
#include "View.h"
#include "util/util.h"

#include <cstddef>
#include <cstdio>
#include <iostream>
#include <ostream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

template <typename T> void ignoreEOFInput(T& var);

DiscardView::DiscardView(Game* game) : View(game) {}

void DiscardView::update()
{
    Game* game                 = getSubject();
    currentPlayer              = game->getCurrentPlayer();
    GameController* controller = GameController::getInstance();
    if (currentPlayer->numberOfPlantsOwned() == 3) {
        controller->queueEvent(this);
    }
}

void DiscardView::show()
{
    size_t index;
    do {
        index = input();
    } while (!confirm(index));

    DiscardPowerplant* command = new DiscardPowerplant(currentPlayer, index);
    GameController* controller = GameController::getInstance();
    controller->invoke(command);
}

void DiscardView::print()
{
    const PropertyManager* powerplants = currentPlayer->getPropertyManager();
    std::string color = colorToString.at(currentPlayer->getColor());

    const PowerplantCard* storing = powerplants->getStoring();

    ArrangeCards lineup;
    lineup.border(true);

    lineup.labels({"1", "2", "3"});
    lineup(powerplants->rbegin(), powerplants->rend());

    clear_term();

    std::cout << "\n"
              << "player: " << color << "\n"
              << "\n"
              << *storing << "\n"
              << "\n"
              << "Currently owned powerplants:"
              << "\n"
              << "\n"
              << lineup << "\n";
}

size_t DiscardView::input()
{
    std::string in;
    size_t index;

    while (true) {
        print();

        std::cout << "\n"
                  << "Select a plant to discard"
                  << "\n"
                  << "enter a number (1-3): ";

        ignoreEOFInput<std::string>(in);

        std::stringstream validator(in);
        if (validator >> index && index > 0 && index < 4) {
            if (index == 1)
                index = 3;
            else if (index == 3)
                index = 1;
            return index;
        }
        if (validator.eof())
            continue;

        std::cerr << "\n"
                  << in << " is invalid input."
                  << "\n";
        std::cout << "Press ENTER to continue";
        std::cin.ignore();
    }
}

bool DiscardView::confirm(size_t index)
{
    std::string color = colorToString.at(currentPlayer->getColor());
    const PowerplantCard* old =
        *(currentPlayer->getPropertyManager()->begin() + index - 1);
    const PowerplantCard* storing =
        currentPlayer->getPropertyManager()->getStoring();

    std::vector<const PowerplantCard*> cards           = {old, storing};
    std::vector<std::array<std::string, 4>> seperators = {{"", "<", ">", ""}};

    ArrangeCards lineup;
    lineup.border(true);
    lineup.labels({"OLD", "NEW"});
    lineup.seperators(seperators);
    lineup(old, storing);

    std::string in;
    char choice;

    while (true) {
        clear_term();

        std::cout << "\n"
                  << "player: " << color << "\n"
                  << "\n"
                  << "Confirm this exchange:"
                  << "\n"
                  << "\n"
                  << lineup << "\n";

        std::cout << "\n"
                  << "Do you want to proceed? ([y]/n): ";

        ignoreEOFInput<std::string>(in);

        if (in.empty())
            in = "y";

        std::stringstream validator(in);
        if (validator >> choice) {
            if (choice == 'y')
                return true;
            else if (choice == 'n')
                return false;
        }

        std::cerr << "\n"
                  << in << " is invalid input."
                  << "\n";
        std::cout << "Press ENTER to continue";
        std::cin.ignore();
    }
}

Game* DiscardView::getSubject() const
{
    return static_cast<Game*>(Observer::getSubject());
}

template <typename T> void ignoreEOFInput(T& var)
{
    if (!getline(std::cin, var)) {
        std::cin.clear();
        std::cout << "\n";
    }
}
