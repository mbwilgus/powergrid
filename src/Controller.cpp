#include "Controller.h"
#include "Command.h"
#include "Presentable.h"

#include <unordered_map>

GameController* GameController::instance = nullptr;

GameController::GameController() {}

GameController::~GameController() {}

GameController* GameController::getInstance()
{
    if (!instance) {
        instance = new GameController();
    }
    return instance;
}

void GameController::deleteInstance()
{
    delete instance;
    instance = nullptr; // Truly necessary here otherewise could not create a
                        // new instance
}

void GameController::invoke(Command* command)
{
    command->execute();
    // TODO: Do other stuff here
    delete command;
    command = nullptr;
}

void GameController::queueEvent(Presentable* event)
{
    eventQueue.push_back(event);
}

void GameController::triggerNextEvent()
{
    if (!eventQueue.empty()) {
        Presentable* event = eventQueue.front();
        eventQueue.pop_front();
        event->show();
    }
}
