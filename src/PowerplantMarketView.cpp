#include "PowerplantMarketView.h"
#include "ArrangeCards.h"
#include "Card.h"
#include "Definition.h"
#include "InputView.h"
#include "Market.h"
#include "Observer.h"
#include "Player.h"
#include "Scene.h"
#include "Subject.h"
#include "Validator.h"
#include "util/util.h"

#include <ios>
#include <iostream>
#include <stdexcept>
#include <string>

PowerplantMarketView::InitialScene::InitialScene(PowerplantMarketView* view)
    : view(view)
{
}

PowerplantMarketView::InitialScene::~InitialScene() {}

void PowerplantMarketView::InitialScene::show()
{
    clear_term();

    std::cout << "Player: " << colorToString.at(view->color) << "     "
              << "Elektro: " << view->elektro;

    std::cout << "\n\n" << view->cards;
}

bool PowerplantMarketView::InitialScene::input(Validator* validator)
{
    std::string input;

    std::cout << "\n\n"
              << "Would you like to auction a powerplant ([y]/n): ";

    while (!getline(std::cin, input)) {
        std::cin.clear();
    }

    try {
        std::string sanitized = validator->validate(input);

        if (sanitized == "y") {
            view->scene = new SelectScene(view);
            delete this;
            return true;
        } else if (sanitized == "n") {
            delete this;
            return false;
        }

    } catch (const std::runtime_error& e) {
        std::cerr << "\n" << e.what() << "\n";
        std::cout << "Press ENTER to continue";
        std::cin.ignore();
    }

    return true;
}

PowerplantMarketView::SelectScene::SelectScene(PowerplantMarketView* view)
    : view(view)
{
}

PowerplantMarketView::SelectScene::~SelectScene() {}

void PowerplantMarketView::SelectScene::show()
{
    clear_term();

    std::cout << "Player: " << colorToString.at(view->color) << "     "
              << "Elektro: " << view->elektro;

    std::cout << "\n\n" << view->cards;
}

bool PowerplantMarketView::SelectScene::input(Validator* validator)
{
    std::string input;

    std::string note = "";
    bool mustBuy     = view->roster->current()->numberOfPlantsOwned() == 0;
    if (mustBuy) {
        note = "You must auction a plant because you do not own any\n";
    }

    std::cout << "\n\n"
              << "Select a plant to auction\n"
              << note << "enter a number (1-4"
              << (mustBuy ? "): " : ", empty cancels): ");

    while (!getline(std::cin, input)) {
        std::cin.clear();
    }

    try {
        std::string sanitized = validator->validate(input);

        if (sanitized.empty()) {
            view->scene = new InitialScene(view);
        } else {
            view->selection = std::stoull(sanitized) - 1;
            view->scene     = new ConfirmScene(view);
        }

        delete this;

    } catch (const std::runtime_error& e) {
        std::cerr << "\n" << e.what() << "\n";
        std::cout << "Press ENTER to continue";
        std::cin.ignore();
    }

    return true;
}

PowerplantMarketView::ConfirmScene::ConfirmScene(PowerplantMarketView* view)
    : view(view)
{
}

PowerplantMarketView::ConfirmScene::~ConfirmScene() {}

void PowerplantMarketView::ConfirmScene::show()
{
    clear_term();

    std::cout << "Player: " << colorToString.at(view->color) << "     "
              << "Elektro: " << view->elektro;

    size_t selection = view->selection;
    std::cout << "\n\n" << *view->market->preview(selection);
}

bool PowerplantMarketView::ConfirmScene::input(Validator* validator)
{
    std::string input;

    std::cout << "\n\n"
              << "Confirm this selection ([y]/n): ";

    while (!getline(std::cin, input)) {
        std::cin.clear();
    }

    try {
        std::string sanitized = validator->validate(input);

        if (sanitized == "y") {
            delete this;
            return false;
        } else if (sanitized == "n") {
            view->scene = new SelectScene(view);
            delete this;
            return true;
        }

    } catch (const std::runtime_error& e) {
        std::cerr << "\n" << e.what() << "\n";
        std::cout << "Press ENTER to continue";
        std::cin.ignore();
    }

    return true;
}

PowerplantMarketView::PowerplantMarketView(PowerplantMarket* market,
                                           Roster* roster)
    : market(market), roster(roster)
{
    market->attach(this);
    roster->attach(this);
}

PowerplantMarketView::~PowerplantMarketView() { delete scene; }

void PowerplantMarketView::show() { scene->show(); }

bool PowerplantMarketView::input(Validator* validator)
{
    bool proceed = scene->input(validator);
    if (!proceed)
        scene = nullptr;
    return proceed;
}

void PowerplantMarketView::update()
{
    const Player* currentPlayer = roster->current();
    color                       = currentPlayer->getColor();
    elektro                     = currentPlayer->getElektro();
    /* bool mustBuy                = currentPlayer->numberOfPlantsOwned() == 0;
     */

    std::stringstream currentMarket;

    currentMarket << *market;

    cards = currentMarket.str();

    delete scene;
    if (currentPlayer->numberOfPlantsOwned() == 0) {
        scene = new SelectScene(this);
    } else {
        scene = new InitialScene(this);
    }
}
