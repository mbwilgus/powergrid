#ifndef __SCENE_H__
#define __SCENE_H__

#include "Presentable.h"

class Scene : public Presentable
{
  public:
    virtual ~Scene();

    virtual void show() override = 0;
};

#endif
