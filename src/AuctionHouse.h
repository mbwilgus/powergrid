#ifndef __AUCTION_HOUSE_H__
#define __AUCTION_HOUSE_H__

#include "Card.h"
#include "Definition.h"
#include "Roster.h"
#include "Subject.h"

#include <utility>

using AuctionResult = std::pair<PowerplantSale*, Player*>;

class AuctionHouse : public Subject
{
  private:
    Roster* roster = nullptr;

    PowerplantCard* auctioning = nullptr;
    Amount currentBid          = 0;

    bool openingBid;

  public:
    AuctionHouse();
    AuctionHouse(const AuctionHouse& source);

    void startAuction(Roster* roster, PowerplantCard* powerplant);

    AuctionResult declareWinner();

    const PowerplantCard* getAuctioning() const;
    Amount getCurrentBid() const;

    bool isOpeningBid() const;

    void placeBid(Amount bid);
};

#endif
