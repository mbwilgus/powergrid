#include "Cart.h"
#include "Commodity.h"
#include "Definition.h"
#include "Market.h"
#include "ResourceType.h"

#include <iostream>

Cart::Cart() noexcept : cart(new ResourceParcel()) {}

void Cart::add(Commodity& commodity, size_t amount) noexcept
{
    cart->add(commodity, amount);

    cost += commodity.quote(amount);
}

void Cart::remove(Commodity& commodity, size_t amount)
{
    // Can throw std::out_of_range
    cart->remove(commodity, amount);

    cost -= commodity.quote(amount);
}

ResourceSale* Cart::makeSale() noexcept
{
    ResourceSale* sale = new ResourceSale(cart, cost);
    cart = nullptr;
    return sale;
}

std::ostream& operator<<(std::ostream& out, const Cart& rhs) noexcept
{
    if (rhs.cart->empty()) {
        out << "Empty";
    }
    return out;
}
