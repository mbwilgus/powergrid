#ifndef __BUYABLE_H__
#define __BUYABLE_H__

class Buyable
{
  public:
    virtual ~Buyable();

    virtual Buyable* clone() = 0;
};

#endif
