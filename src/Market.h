#ifndef __MARKET_H__
#define __MARKET_H__

#include "Buyable.h"
#include "Card.h"
#include "Cart.h"
#include "Commodity.h"
#include "Deck.h"
#include "Definition.h"
#include "ResourceType.h"
#include "Subject.h"

#include <array>
#include <cstddef> // For size_t
#include <iostream>
#include <ostream>
#include <string>
#include <unordered_map>
#include <vector>

class Buyer;

class Sale
{
  private:
    Buyable* item;
    Elektro price;

  public:
    Sale(Buyable* item, const Elektro price);
    Sale(const Sale& source);
    Sale(Sale&& source);

    virtual ~Sale();

    virtual Buyable* claim(Buyer* buyer);

    Sale& operator=(Sale source);

    friend void swap(Sale& first, Sale& second);
};

class PowerplantSale : public Sale
{
  public:
    PowerplantSale(PowerplantCard* powerplant, Elektro price);
    PowerplantSale(const PowerplantSale& source);
    PowerplantSale(PowerplantSale&& source);

    virtual ~PowerplantSale();

    virtual PowerplantCard* claim(Buyer* buyer) override;

    PowerplantSale& operator=(const PowerplantSale& source);
};

class ResourceParcel : public Buyable
{
  private:
    std::unordered_map<ResourceType, Amount> resources;

  public:
    ResourceParcel();

    virtual ~ResourceParcel();

    virtual ResourceParcel* clone() override;

    void add(const ResourceType& resource, const size_t amount);
    void remove(const ResourceType& resource, const size_t amount);

    bool empty() const;
};

class ResourceSale : public Sale
{
  public:
    ResourceSale(ResourceParcel* resources, Elektro price);
    ResourceSale(const ResourceSale& source);
    ResourceSale(ResourceSale&& source);

    virtual ~ResourceSale();
    virtual ResourceParcel* claim(Buyer* buyer) override;

    ResourceSale& operator=(const ResourceSale& source);
};

class Market : public Subject
{
  public:
    virtual ~Market();

    virtual void checkout(Buyer* buyer) = 0;
};

class PowerplantMarket : public Market
{
  private:
    Deck deck;
    bool step3Drawn = false;
    std::array<PowerplantCard*, 8> market;
    Amount powerplantsOnMarket = 0;

    PowerplantSale* sale = nullptr;

    void sortMarket();

  public:
    PowerplantMarket();
    PowerplantMarket(Deck& deck);
    PowerplantMarket(const PowerplantMarket& source);
    PowerplantMarket(PowerplantMarket&& source);

    virtual ~PowerplantMarket();

    void ringup(PowerplantSale* sale);
    virtual void checkout(Buyer* buyer) override;

    Amount checkStock() const;

    PowerplantCard* select(Index selection);

    const PowerplantCard* preview(Index selection) const;

    PowerplantMarket& operator=(PowerplantMarket source);

    friend void swap(PowerplantMarket& first, PowerplantMarket& second);
    friend std::ostream& operator<<(std::ostream& out,
                                    const PowerplantMarket& rhs);
};

class ResourceMarket : public Market
{
  private:
    std::unordered_map<ResourceType, Commodity> commodities;

    Cart* cart = nullptr;

    ResourceSale* sale = nullptr;

  public:
    ResourceMarket();
    ResourceMarket(const ResourceMarket& source);
    ResourceMarket(ResourceMarket&& source);

    virtual ~ResourceMarket();

    void peruse(Cart* cart);
    void ringup(ResourceSale* sale);
    virtual void checkout(Buyer* buyer) override;

    void take(const ResourceType& resource, size_t amount);
    void putBack(const ResourceType& resource, size_t amount);
    void stock(const ResourceType& resource, size_t amount);
    void recycle(const ResourceType& resource, size_t amount);

    ResourceMarket& operator=(ResourceMarket source);

    friend void swap(ResourceMarket& first, ResourceMarket& second);
    friend std::ostream& operator<<(std::ostream& out, const ResourceMarket& rhs);
};

#endif
