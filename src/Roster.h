#ifndef __ROSTER_H__
#define __ROSTER_H__

#include "Player.h"
#include "Subject.h"

#include <stack>
#include <vector>

class Roster : public Subject
{
  private:
    class RosterState
    {
      public:
        std::deque<Player*> roster;
        Player* current = nullptr;
        std::vector<Player*> bench;
        bool played = false;
    };

    RosterState state;
    std::stack<RosterState> saved;

    bool auctionMode = false;

  public:
    Roster();
    Roster(std::vector<Player*>& players);

    ~Roster();

    void order();
    void reverse();

    void save();
    void restore();

    bool played() const;

    Player* current();
    const Player* current() const;
    Player* next();

    void bench(Player* player);

    void toggleAuctionMode();
};

#endif
