#include "Subject.h"
#include "Observer.h"

#include <algorithm>
#include <vector>

void Subject::attach(Observer* observer) { observers.push_back(observer); }

void Subject::detach(Observer* observer)
{
    auto pos = std::remove(observers.begin(), observers.end(), observer);
    observers.erase(pos, observers.end());
}

void Subject::notify()
{
    for (auto& observer : observers) {
        observer->update();
    }
}
