#ifndef __VALIDATOR_H__
#define __VALIDATOR_H__

#include "Stockpile.h"
#include "View.h"

#include <stdexcept>
#include <string>
#include <unordered_map>

class ValidationError : public std::runtime_error
{
  public:
    ValidationError(const std::string& msg);
    ValidationError(const char* msg);
};

class Validator
{
  public:
    virtual ~Validator();

    virtual std::string validate(const std::string& input) = 0;
};

class SelectPowerplantValidator : public Validator
{
  private:
    class State
    {
      public:
        virtual ~State();

        virtual std::string validate(const std::string& input) = 0;
    };

    class InitialState : public State
    {
      private:
        SelectPowerplantValidator* validator;

      public:
        InitialState(SelectPowerplantValidator* validator);

        virtual ~InitialState();

        virtual std::string validate(const std::string& input) override;
    };

    class SelectState : public State
    {
      private:
        SelectPowerplantValidator* validator;

      public:
        SelectState(SelectPowerplantValidator* validator);

        virtual ~SelectState();

        virtual std::string validate(const std::string& input) override;
    };

    class ConfirmState : public State
    {
      private:
        SelectPowerplantValidator* validator;

      public:
        ConfirmState(SelectPowerplantValidator* validator);

        virtual ~ConfirmState();

        virtual std::string validate(const std::string& input) override;
    };

    size_t* selection;
    size_t limit;
    bool mustBuy;

    State* state;

  public:
    SelectPowerplantValidator(size_t& index, size_t limit, bool mustBuy);

    virtual ~SelectPowerplantValidator();

    virtual std::string validate(const std::string& input) override;
};

class BidValidator : public Validator
{
  private:
    class State
    {
      public:
        virtual ~State();

        virtual std::string validate(const std::string& input) = 0;
    };

    class InitialState : public State
    {
      private:
        BidValidator* validator;

      public:
        InitialState(BidValidator* validator);

        virtual ~InitialState();

        virtual std::string validate(const std::string& input) override;
    };

    class BidState : public State
    {
      private:
        BidValidator* validator;

      public:
        BidState(BidValidator* validator);

        virtual ~BidState();

        virtual std::string validate(const std::string& input) override;
    };

    size_t* bid;
    size_t min;
    size_t max;
    bool mustBid;

    State* state;

  public:
    BidValidator(size_t& bid, size_t min, size_t max, bool mustBid);

    virtual ~BidValidator();

    virtual std::string validate(const std::string& input) override;
};

class ResourcePurchaseValidator : public Validator
{
  private:
    class State
    {
      public:
        virtual ~State();

        virtual std::string validate(const std::string& input) = 0;
    };

    class InitialState : public State
    {
      private:
        ResourcePurchaseValidator* validator;

      public:
        InitialState(ResourcePurchaseValidator* validator);

        virtual ~InitialState();

        virtual std::string validate(const std::string& input) override;
    };

    size_t* amount;
    ResourceQuota quotas;
    ResourceType resource;
    bool remove = false;

    bool act_ = false;

    State* state;

  public:
    ResourcePurchaseValidator(size_t& amount, ResourceQuota& quotas);

    virtual ~ResourcePurchaseValidator();

    virtual std::string validate(const std::string& input) override;

    ResourceType getResource() const;

    bool forRemoval() const;

    bool act() const;
};

class InvalidValidator : public Validator
{
  public:
    virtual std::string validate(const std::string& input) override;
};

#endif
