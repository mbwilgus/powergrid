#ifndef __AUCTION_HOUSE_VIEW_H__
#define __AUCTION_HOUSE_VIEW_H__

#include "AuctionHouse.h"
#include "Card.h"
#include "Definition.h"
#include "InputScene.h"
#include "InputView.h"
#include "Player.h"
#include "Roster.h"
#include "Validator.h"
#include <string>

class AuctionHouseView : public InputView
{
  private:
    class InitialScene : public InputScene
    {
      private:
        AuctionHouseView* view;

      public:
        InitialScene(AuctionHouseView* view);

        virtual ~InitialScene();

        virtual void show() override;
        virtual bool input(Validator* validator) override;
    };

    class BidScene : public InputScene
    {
      private:
        AuctionHouseView* view;

      public:
        BidScene(AuctionHouseView* view);

        virtual ~BidScene();

        virtual void show() override;
        virtual bool input(Validator* validator) override;
    };

    const AuctionHouse* auctionHouse;
    const Roster* roster;

    Color color;
    size_t elektro;

    const PowerplantCard* auctioning;

    std::string bid;

    InputScene* scene = nullptr;

  public:
    AuctionHouseView(AuctionHouse* auctionHouse, Roster* roster);

    virtual ~AuctionHouseView();

    virtual void show() override;
    virtual bool input(Validator* validator) override;
    virtual void update() override;

    void announceWinner(const PowerplantCard* powerplant,
                        const Player* winner) const;
};

#endif
