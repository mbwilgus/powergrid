#ifndef __STOCKPILE_H__
#define __STOCKPILE_H__

#include "Definition.h"
#include "ResourceType.h"

#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <utility> // for swap

using ResourceSet = std::unordered_set<ResourceType>;
using ResourceQuota = std::unordered_map<ResourceType, Amount>;

class Stockpile
{
  private:
    using Storage = std::unordered_map<ResourceType, std::pair<Amount, Limit>>;
    using ResourceGraph = std::unordered_map<ResourceType, ResourceSet>;

    Storage stored;
    ResourceGraph relatedResources;

  public:
    Stockpile();
    Stockpile(const Stockpile& source);
    Stockpile(Stockpile&& source);

    Limit getStorageLimit(const ResourceType& resource) const;
    Amount getAmountAvailable(const ResourceType& resource) const;
    Amount getStorageSpace(const ResourceType& resource) const;
    ResourceQuota getResourceQuotas() const;
    bool canStore(const ResourceType& resource) const;

    void increaseStorageLimit(const ResourceType& resource,
                              const Amount amount);
    void decreaseStorageLimit(const ResourceType& resource,
                              const Amount amount);

    void store(const ResourceType& resource, const Amount amount);
    void withdraw(const ResourceType& resource, const Amount amount);

    const ResourceSet& getRelatedResources(const ResourceType& resource) const;
    /* const ResourceSet::const_iterator begin(const ResourceType& resource)
     * const; */

    void relateResources(const ResourceType& first, const ResourceType& second);

    Stockpile& operator=(Stockpile source);

    friend std::ostream& operator<<(std::ostream& out,
                                    const Stockpile& stockpile);
    friend void swap(Stockpile& first, Stockpile& second);
};

#endif
