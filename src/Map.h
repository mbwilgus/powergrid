#ifndef __MAP_H__
#define __MAP_H__

// #include "../../graph/src/Graph.h"
#include "City.h"

#include <string>

class Map
{
  private:
    // Graph<std::string, City*> map;

  public:
    Map();
    Map(const Map& source);
    Map(Map&& source);

    ~Map();

    int getSize() const;

    Map& operator=(const Map& source);
};

#endif
