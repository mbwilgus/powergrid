#ifndef __COMMODITY_H__
#define __COMMODITY_H__

#include "ResourceType.h"

class Forcaster;

class Commodity
{
  private:
    ResourceType resource;
    size_t supply;
    size_t poolSize;
    size_t pool;
    size_t volatility;

    Forcaster* forcaster = nullptr;

  public:
    Commodity();
    Commodity(const ResourceType& resource, size_t supply, size_t poolSize,
              size_t volatility);
    Commodity(const Commodity& source);
    ~Commodity();

    void take(size_t amount);
    void putBack(size_t amount);
    void stock(size_t amount);
    void recycle(size_t amount);

    size_t currentMarketPrice() const;
    size_t quote(size_t amount) const;

    Commodity& operator=(Commodity source);
    // TODO: Add move assignment operator here

    operator ResourceType() const;

    friend void swap(Commodity& first, Commodity& second);
    friend std::ostream& operator<<(std::ostream& out,
                                    const Commodity& commodity);
};

class Forcaster
{
  private:
    size_t threshold;

  public:
    Forcaster();
    Forcaster(size_t threshold);
    Forcaster(const Forcaster& source);
    virtual ~Forcaster();

    virtual Forcaster* clone() const = 0;

    size_t getThreshold() const;

    virtual size_t forcast(size_t supply, size_t poolSize,
                           size_t volatility) const = 0;
};

class DefaultForcaster : public Forcaster
{
  public:
    DefaultForcaster();
    DefaultForcaster(size_t threshold);
    DefaultForcaster(const DefaultForcaster& source);
    virtual ~DefaultForcaster();

    virtual DefaultForcaster* clone() const override;

    virtual size_t forcast(size_t supply, size_t poolSize,
                           size_t volatility) const override;
};

#endif
