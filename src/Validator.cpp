#include "Validator.h"
#include "ResourceType.h"
#include "Stockpile.h"

#include <algorithm>
#include <cctype>
#include <sstream>
#include <stdexcept>
#include <string>

namespace
{
std::string tolower(const std::string& original)
{
    std::string lower;
    lower.resize(original.length());
    std::transform(original.begin(), original.end(), lower.begin(),
                   [&](char c) { return std::tolower(c); });
    return lower;
}
} // namespace

ValidationError::ValidationError(const std::string& msg)
    : std::runtime_error(msg)
{
}

ValidationError::ValidationError(const char* msg) : std::runtime_error(msg) {}

Validator::~Validator() {}

SelectPowerplantValidator::State::~State() {}

SelectPowerplantValidator::InitialState::InitialState(
    SelectPowerplantValidator* validator)
    : validator(validator)
{
}

SelectPowerplantValidator::InitialState::~InitialState() {}

std::string
SelectPowerplantValidator::InitialState::validate(const std::string& input)
{
    std::string sanitized = tolower(input);

    if (sanitized == "n") {
        delete this;
        return sanitized;

    } else if (sanitized == "y" || sanitized == "") {
        validator->state = new SelectState(validator);
        delete this;
        return "y";
    }

    throw ValidationError(input + " is invalid input");
}

SelectPowerplantValidator::SelectState::SelectState(
    SelectPowerplantValidator* validator)
    : validator(validator)
{
}

SelectPowerplantValidator::SelectState::~SelectState() {}

std::string
SelectPowerplantValidator::SelectState::validate(const std::string& input)
{
    std::string sanitized = tolower(input);

    if (sanitized == "") {
        if (validator->mustBuy) {
            throw ValidationError("You must auction a powerplant");
        }

        validator->state = new InitialState(validator);
        delete this;
        return sanitized;
    }

    std::stringstream selector(sanitized);
    size_t selection;

    if (selector >> selection) {
        if (selection >= 1 && selection <= 4) {
            *validator->selection = selection - 1;
            validator->state      = new ConfirmState(validator);
            delete this;
            return sanitized;
        } else if (selection >= 4 && selection <= 8) {
            const char* msg = "Only powerplants on the actual market (1-4) are "
                              "available for acution";
            throw ValidationError(msg);
        }
    }

    throw ValidationError(input + " is invalid input");
}

SelectPowerplantValidator::ConfirmState::ConfirmState(
    SelectPowerplantValidator* validator)
    : validator(validator)
{
}

SelectPowerplantValidator::ConfirmState::~ConfirmState() {}

std::string
SelectPowerplantValidator::ConfirmState::validate(const std::string& input)
{
    std::string sanitized = tolower(input);

    if (sanitized == "n") {
        validator->state = new SelectState(validator);
        delete this;
        return sanitized;

    } else if (sanitized == "y" || sanitized == "") {
        delete this;
        return "y";
    }

    throw ValidationError(input + " is invalid input");
}

SelectPowerplantValidator::SelectPowerplantValidator(size_t& index,
                                                     size_t limit, bool mustBuy)
    : selection(&index), limit(limit), mustBuy(mustBuy)
{
    if (mustBuy) {
        state = new SelectState(this);
    } else {
        state = new InitialState(this);
    }

    index = 8;
}

SelectPowerplantValidator::~SelectPowerplantValidator() {}

std::string SelectPowerplantValidator::validate(const std::string& input)
{
    return state->validate(input);
}

BidValidator::State::~State() {}

BidValidator::InitialState::InitialState(BidValidator* validator)
    : validator(validator)
{
}

BidValidator::InitialState::~InitialState() {}

std::string BidValidator::InitialState::validate(const std::string& input)
{
    std::string sanitized = tolower(input);

    if (sanitized == "n") {
        delete this;
        return sanitized;

    } else if (input == "y" || input == "") {
        validator->state = new BidState(validator);
        delete this;
        return "y";
    }

    throw ValidationError(input + " is invalid input");
}

BidValidator::BidState::BidState(BidValidator* validator) : validator(validator)
{
}

BidValidator::BidState::~BidState() {}

std::string BidValidator::BidState::validate(const std::string& input)
{
    std::string sanitized = tolower(input);

    if (sanitized == "c") {
        if (validator->mustBid) {
            std::string msg = "You must make a bid because "
                              "you started this auction";
            throw ValidationError(msg);
        }

        validator->state = new InitialState(validator);
        delete this;
        return sanitized;
    }

    if (sanitized.empty()) {
        sanitized       = std::to_string(validator->min);
        *validator->bid = validator->min;
        delete this;
        return sanitized;
    }

    std::stringstream selector(sanitized);
    size_t bid;

    if (selector >> bid) {
        if (bid < validator->min) {
            throw ValidationError(
                "You must bid at least the minimum indicated");
        } else if (bid > validator->max) {
            throw ValidationError(
                "You do not have enough money to make this bid");
        } else {
            *validator->bid = bid;
            delete this;
            return sanitized;
        }
    }

    throw ValidationError(input + " is invalid input");
}

BidValidator::BidValidator(size_t& bid, size_t min, size_t max, bool mustBid)
    : bid(&bid), min(min), max(max), mustBid(mustBid)
{
    if (mustBid) {
        state = new BidState(this);
    } else {
        state = new InitialState(this);
    }
    bid = 0;
}

BidValidator::~BidValidator() {}

std::string BidValidator::validate(const std::string& input)
{
    return state->validate(input);
}

ResourcePurchaseValidator::State::~State() {}

ResourcePurchaseValidator::InitialState::InitialState(
    ResourcePurchaseValidator* validator)
    : validator(validator)
{
}

ResourcePurchaseValidator::InitialState::~InitialState() {}

std::string
ResourcePurchaseValidator::InitialState::validate(const std::string& input)
{
    std::string sanitized = tolower(input);

    if (sanitized == "n") {
        delete this;
        return sanitized;

    } else if (input == "y" || input == "") {
        delete this;
        return "y";
    }

    throw ValidationError(input + " is invalid input");
}

ResourcePurchaseValidator::ResourcePurchaseValidator(size_t& amount, ResourceQuota& quotas)
    : amount(&amount), quotas(quotas)
{
    state = new InitialState(this);
}

ResourcePurchaseValidator::~ResourcePurchaseValidator() {}

std::string ResourcePurchaseValidator::validate(const std::string& input)
{
    return state->validate(input);
}

ResourceType ResourcePurchaseValidator::getResource() const
{
    return resource;
}

bool ResourcePurchaseValidator::forRemoval() const
{
    return remove;
}

bool ResourcePurchaseValidator::act() const
{
    return act_;
}

std::string InvalidValidator::validate(const std::string& input)
{
    throw ValidationError(input + " is invalid input");
}
