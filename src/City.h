#ifndef __CITY_H__
#define __CITY_H__

#include <array>
#include <string>

class Player;

class City
{
  private:
    std::string name;
    size_t region;
    size_t buildingCost = 10;

    std::array<Player*, 3> owners;
    size_t population = 0;

  public:
    City();
    City(const std::string& name, const int region);
    City(const City& source);
    City(City&& source);
    ~City();

    bool build(const Player& player);

    std::string getName() const;

    City& operator=(City source);

    friend void swap(City& first, City& second);
};

void swap(City& first, City& second);

#endif
