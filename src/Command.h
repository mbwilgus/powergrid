#ifndef __COMMAND_H__
#define __COMMAND_H__

#include "AuctionHouse.h"
#include "Cart.h"
#include "Market.h"
#include "Player.h"

class Command
{
  public:
    virtual ~Command();
    virtual void execute() = 0;
};

class SelectPowerplant : public Command
{
  private:
    PowerplantMarket* receiver;
    size_t arg;

  public:
    SelectPowerplant(PowerplantMarket* receiver, size_t arg);
    virtual void execute() override;
};

class DiscardPowerplant : public Command
{
  private:
    Player* reciever;
    size_t arg;

  public:
    DiscardPowerplant(Player* receiver, size_t arg);
    virtual void execute() override;
};

class PlaceBid : public Command
{
  private:
    AuctionHouse* receiver;
    size_t arg;

  public:
    PlaceBid(AuctionHouse* receiver, size_t arg);
    virtual void execute() override;
};

class BuyResources : public Command
{
  private:
    ResourceMarket* receiver;
    Cart arg;

  public:
    BuyResources(ResourceMarket* receiver, Cart& cart);
    virtual void execute() override;
};

#endif
