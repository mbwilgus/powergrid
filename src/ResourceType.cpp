#include "ResourceType.h"
#include "Log.h"

#include <algorithm> // For sort
#include <cstddef>
#include <fstream>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/value.h>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

size_t
std::hash<ResourceType>::operator()(const ResourceType& type) const noexcept
{
    return std::hash<size_t>()(type._index);
}

ResourceType::ResourceType() {}

ResourceType::ResourceType(size_t index, const std::string& name)
    : _index(index), _name(name)
{
}

const std::string& ResourceType::name() const { return _name; }

bool ResourceType::operator==(const ResourceType& rhs) const
{
    return !operator!=(rhs);
}

bool ResourceType::operator!=(const ResourceType& rhs) const
{
    return _index != rhs._index;
}

bool ResourceType::operator<(const ResourceType& rhs) const
{
    return _index < rhs._index;
}

InvalidResourceError::InvalidResourceError(const char* msg)
    : std::runtime_error(msg)
{
}

void ResourceTypesBuilder::initializeResources(const std::string& filename,
                                               const Log* logger)
{
    Json::Value root;

    try {
        std::ifstream in(filename);
        in.exceptions(std::ifstream::failbit | std::ifstream::badbit);

        in >> root;
    } catch (const std::ifstream::failure& e) {
        logger->log("ERROR: Could not open/read \"" + filename + "\"");
    }

    ResourceTypes::_types.clear();

    Json::Value types = root["types"];
    Json::Value props = root["properties"];

    for (auto& key : types) {
        std::string name = key.asString();
        if (!props[name].isNull()) {
            size_t index         = props[name]["index"].asLargestUInt();
            size_t poolSize      = props[name]["poolSize"].asLargestUInt();
            size_t initialSupply = props[name]["supply"].asLargestUInt();
            size_t volatility    = props[name]["volatility"].asLargestUInt();
            size_t scarcityThreshold =
                props[name]["scarcityThreshold"].asLargestUInt();

            ResourceType resource(index, name);

            ResourceTypes::resourceProperties[resource][Property::poolSize] =
                poolSize;
            ResourceTypes::resourceProperties[resource]
                                             [Property::initialSupply] =
                                                 initialSupply;
            ResourceTypes::resourceProperties[resource][Property::volatility] =
                volatility;
            ResourceTypes::resourceProperties[resource]
                                             [Property::scarcityThreshold] =
                                                 scarcityThreshold;

            ResourceTypes::_items++;
            ResourceTypes::_types.push_back(resource);
            ResourceTypes::_resourceMap[name] = resource;
        }
    }

    std::sort(ResourceTypes::_types.begin(), ResourceTypes::_types.end());
}

size_t ResourceTypes::_items = 0;
std::vector<ResourceType> ResourceTypes::_types;
std::unordered_map<std::string, ResourceType> ResourceTypes::_resourceMap;
std::unordered_map<ResourceType, std::unordered_map<Property, size_t>>
    ResourceTypes::resourceProperties;

ResourceTypes::ResourceTypes() {}

size_t ResourceTypes::size() { return ResourceTypes::_items; }

const ResourceTypes ResourceTypes::enumerate() { return ResourceTypes(); }

const ResourceType& ResourceTypes::get(const std::string& name)
{
    try {
        return _resourceMap.at(name);
    } catch (const std::out_of_range& e) {
        throw InvalidResourceError(
            (name + " is not a valid resource name").c_str());
    }
}

size_t ResourceTypes::getProperty(const ResourceType& resource,
                                  const Property property)
{
    return resourceProperties[resource][property];
}

std::vector<std::pair<ResourceType, ResourceType>>
ResourceTypes::getResourcePairs(const std::vector<ResourceType>& types)
{
    std::vector<std::pair<ResourceType, ResourceType>> pairs;

    if (!types.empty()) {
        std::for_each(types.begin(), types.end() - 1,
                      [&](const ResourceType& first) {
                          std::for_each(types.begin() + 1, types.end(),
                                        [&](const ResourceType& second) {
                                            pairs.push_back(
                                                std::make_pair(first, second));
                                        });
                      });
    }

    return pairs;
}

ResourceTypes::ResourceTypeIterator ResourceTypes::begin()
{
    return ResourceTypes::ResourceTypeIterator(&ResourceTypes::_types[0]);
}

ResourceTypes::ResourceTypeIterator ResourceTypes::end()
{
    return ResourceTypes::ResourceTypeIterator(
        &ResourceTypes::_types[ResourceTypes::_types.size()]);
}
