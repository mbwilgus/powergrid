#ifndef __DEFINITION_H__
#define __DEFINITION_H__

#include <cstddef>
#include <string>
#include <unordered_map>

using Index   = size_t;
using Amount  = size_t;
using Cost    = size_t;
using Value   = size_t;
using Elektro = size_t; // Elektro is players money. Players cannot be in debt
using Limit   = size_t;

enum class Color { red, blue, green };

const std::unordered_map<Color, std::string> colorToString(
    {{Color::blue, "Blue"}, {Color::green, "Green"}, {Color::red, "Red"}});

#endif
