#include "Log.h"

#include <iostream>

#define _P_UNUSED_ __attribute__((unused))

void NullLog::log(_P_UNUSED_ const std::string& message) const {}

void ConsoleLog::log(const std::string& message) const
{
    std::cerr << message << std::endl;
}
