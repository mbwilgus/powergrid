#include "Deck.h"
#include "Card.h"
#include "Log.h"
#include "ResourceType.h"

#include <algorithm> // For shuffle
#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include <jsoncpp/json/json.h>
#include <list>
#include <random> // For default_raondom_engine
#include <string>
#include <utility> // For swap
#include <vector>

Deck DeckBuilder::initializeDeck(const std::string& filename, size_t first,
                                 const Log* logger)
{
    Json::Value root;

    try {
        std::ifstream in(filename);
        in.exceptions(std::ifstream::failbit | std::ifstream::badbit);

        in >> root;
    } catch (const std::ifstream::failure& e) {
        logger->log("Error: Could not open/read \"" + filename + "\"");
    }

    std::vector<PowerplantCard*> initialCards;
    std::vector<PowerplantCard*> otherCards;

    PowerplantCard* top;

    for (auto& key : root) {
        size_t number    = key["number"].asLargestUInt();
        size_t needed    = key["needed"].asLargestUInt();
        size_t powerable = key["powerable"].asLargestUInt();

        PowerplantBuilder builder(number, needed, powerable);

        for (const auto& resource : key["resources"]) {
            try {
                builder.fueledBy(ResourceTypes::get(resource.asString()));
            } catch (const InvalidResourceError& e) {
                logger->log("ERROR: " + std::string(e.what()));
            }
        }

        PowerplantCard* powerplant = builder.getPowerplant();

        if (number < 11) {
            initialCards.push_back(powerplant);
        } else if (number == first) {
            top = powerplant;
        } else {
            otherCards.push_back(powerplant);
        }
    }

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle(otherCards.begin(), otherCards.end(),
                 std::default_random_engine(seed));

    std::vector<PowerplantCard*> powerplants;

    std::move(initialCards.begin(), initialCards.end(),
              std::back_inserter(powerplants));

    powerplants.push_back(top);

    std::move(otherCards.begin(), otherCards.end(),
              std::back_inserter(powerplants));

    Deck deck;
    deck.deck = std::list<Card*>(powerplants.begin(), powerplants.end());
    deck.deck.push_back(new Step3Card());

    return deck;
}

Deck::Deck() {}

Deck::Deck(const Deck& source)
{
    for (const auto& card : source.deck) {
        if (card->isStep3Card()) {
            deck.push_back(new Step3Card());
        } else {
            PowerplantCard* powerplant = static_cast<PowerplantCard*>(card);
            deck.push_back(new PowerplantCard(*powerplant));
        }
    }
}

Deck::Deck(Deck&& source) : deck(std::move(source.deck)) {}

Deck::~Deck()
{
    for (auto& card : deck) {
        delete card;
        card = nullptr;
    }
}

bool Deck::empty() const { return deck.empty(); }

Card* Deck::draw()
{
    Card* card = deck.front();
    deck.pop_front();
    return card;
}

void Deck::discard(Card* const card) { deck.push_back(card); }

// TODO: Rethink shuffling a linked list
void Deck::shuffle() {}

Deck& Deck::operator=(Deck source)
{
    swap(*this, source);

    return *this;
}

void swap(Deck& first, Deck& second)
{
    using std::swap;

    swap(first.deck, second.deck);
}
