#ifndef __PROPERTY_MANAGER_H__
#define __PROPERTY_MANAGER_H__

#include "Card.h"
#include "Definition.h"
#include "Subject.h"

#include <array>

class PropertyManager : public Subject
{
  private:
    std::array<PowerplantCard*, 3> powerplants;
    Amount owned = 0;

    PowerplantCard* storing = nullptr;

  public:
    PropertyManager();
    PropertyManager(const PropertyManager& source);
    PropertyManager(PropertyManager&& source);

    ~PropertyManager();

    Amount numberOfPlantsOwned() const;

    Value highestValuedPlant() const;

    const PowerplantCard* getStoring() const;

    void claimProperty(PowerplantCard* powerplant);
    PowerplantCard* relinquishProperty(const Index index);

    typedef std::array<PowerplantCard*, 3>::const_iterator const_iterator;
    const_iterator begin() const;
    const_iterator end() const;

    typedef std::array<PowerplantCard*, 3>::const_reverse_iterator
        const_reverse_iterator;
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;

    PropertyManager& operator=(PropertyManager source);

    friend void swap(PropertyManager& first, PropertyManager& second);
};

#endif
