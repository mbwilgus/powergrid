#ifndef __INPUT_VIEW_H__
#define __INPUT_VIEW_H__

#include "InputEvent.h"
#include "Subject.h"
#include "Validator.h"
#include "View.h"

class InputView : public View, public InputEvent
{
  public:
    virtual ~InputView();

    virtual void show() override = 0;

    virtual bool input(Validator* validator) override = 0;

    virtual void update() override = 0;
};

#endif
