#include "Roster.h"
#include "Definition.h"
#include "Player.h"

#include <algorithm>
#include <chrono>
#include <random>
#include <vector>

Roster::Roster() {}

Roster::Roster(std::vector<Player*>& players)
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    std::shuffle(players.begin(), players.end(),
                 std::default_random_engine(seed));

    state.roster = std::deque<Player*>(players.begin(), players.end());
    state.roster.push_back(new NullPlayer());
}

Roster::~Roster()
{
    for (Player* player : state.roster) {
        delete player;
        player = nullptr;
    }
    state.roster.clear();

    for (Player* player : state.bench) {
        delete player;
        player = nullptr;
    }
    state.bench.clear();
}

void Roster::order()
{
    auto comperator = [](Player* lhs, Player* rhs) { return *lhs > *rhs; };
    std::sort(state.roster.begin(), state.roster.end(), comperator);
    state.current = nullptr;
}

void Roster::reverse()
{
    std::reverse(state.roster.begin(), state.roster.end() - 1);
    state.current = nullptr;
}

void Roster::save() { saved.push(state); }

void Roster::restore()
{
    state = saved.top();
    saved.pop();
}

bool Roster::played() const
{
    if (auctionMode) {
        return state.roster.size() == 2;
    } else {
        return state.played;
    }
}

Player* Roster::current() { return state.current; }
const Player* Roster::current() const { return state.current; }

Player* Roster::next()
{
    state.current = state.roster.front();
    state.roster.pop_front();

    if (dynamic_cast<NullPlayer*>(state.current)) {
        state.played = true;
        state.roster.push_back(state.current);
        state.current = state.roster.front();
        state.roster.pop_front();
    }

    state.roster.push_back(state.current);

    notify();

    return state.current;
}

void Roster::bench(Player* player)
{
    std::deque<Player*>::iterator pos =
        std::find(state.roster.begin(), state.roster.end(), player);

    state.bench.push_back(*pos);
    state.roster.erase(pos);

    if (*pos == state.current) {
        state.current = nullptr;
    }

    if (state.roster.size() == 1) {
        state.played = true;
    }
}

void Roster::toggleAuctionMode() { auctionMode = !auctionMode; }
