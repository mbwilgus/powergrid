#include "City.h"
#include "Player.h"

#include <algorithm> // For swap

City::City() {}

// NOTE: Put parameterized constructor here

City::City(const City& source)
{
    this->name         = source.name;
    this->region       = source.region;
    this->buildingCost = source.buildingCost;
    for (size_t i = 0; i < source.population; i++) {
        this->owners[i] = new Player(*source.owners[i]);
    }
}

// Note: Put move constructor here

City::~City()
{
    // Don't own players so lets not delete them
}

City& City::operator=(City source)
{
    swap(*this, source);
    return *this;
}

void swap(City& first, City& second)
{
    using std::swap;

    swap(first.name, second.name);
    swap(first.region, second.region);
    swap(first.buildingCost, second.buildingCost);
    swap(first.owners, second.owners);
}
