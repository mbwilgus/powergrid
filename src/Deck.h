#ifndef __DECK_H__
#define __DECK_H__

#include "Card.h"
#include "Log.h"

#include <list>
#include <string>
#include <utility> // For swap

class Deck;

class DeckBuilder
{
  public:
    static Deck initializeDeck(const std::string& filename, size_t first,
                               const Log* logger);
};

class Deck
{
  private:
    std::list<Card*> deck;

  public:
    Deck();
    Deck(const Deck& source);
    Deck(Deck&& source);
    virtual ~Deck();

    bool empty() const;
    Card* draw();
    void discard(Card* const card);

    void shuffle();

    Deck& operator=(Deck source);

    friend class DeckBuilder;

    friend void swap(Deck& first, Deck& second);
};

#endif
