#ifndef __POWERPLANT_MARKET_VIEW_H__
#define __POWERPLANT_MARKET_VIEW_H__

#include "Definition.h"
#include "InputScene.h"
#include "InputView.h"
#include "Market.h"
#include "Observer.h"
#include "Player.h"
#include "Roster.h"
#include "Scene.h"
#include "Validator.h"

#include <string>

class PowerplantMarketView : public InputView
{
  private:
    class InitialScene : public InputScene
    {
      private:
        PowerplantMarketView* view;

      public:
        InitialScene(PowerplantMarketView* view);

        virtual ~InitialScene();

        virtual void show() override;
        virtual bool input(Validator* validator) override;
    };

    class SelectScene : public InputScene
    {
      private:
        PowerplantMarketView* view;

      public:
        SelectScene(PowerplantMarketView* view);

        virtual ~SelectScene();

        virtual void show() override;
        virtual bool input(Validator* validator) override;
    };

    class ConfirmScene : public InputScene
    {
      private:
        PowerplantMarketView* view;

      public:
        ConfirmScene(PowerplantMarketView* view);

        virtual ~ConfirmScene();

        virtual void show() override;
        virtual bool input(Validator* validator) override;
    };

    const PowerplantMarket* market;
    const Roster* roster;

    Color color;
    size_t elektro;

    std::string cards;

    InputScene* scene = nullptr;

    size_t selection;

  public:
    PowerplantMarketView(PowerplantMarket* market, Roster* roster);

    virtual ~PowerplantMarketView();

    virtual void show() override;
    virtual bool input(Validator* validator) override;

    virtual void update() override;
};

#endif
