#ifndef __LOG_H__
#define __LOG_H__

#include <string>

#define _P_UNUSED_ __attribute__((unused))

class Log
{
  public:
    virtual ~Log()                                     = default;
    virtual void log(const std::string& message) const = 0;
};

class NullLog : public Log
{
  public:
    NullLog() = default;
    virtual void log(_P_UNUSED_ const std::string& message) const override;
};

class ConsoleLog : public Log
{
  public:
    ConsoleLog() = default;
    virtual void log(const std::string& message) const override;
};

#endif
