#ifndef __ARRANGE_CARDS_H__
#define __ARRANGE_CARDS_H__

#include "Card.h"
#include "util/traits.h"

#include <cstddef>
#include <ostream>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

class ArrangeCards
{
  private:
    using ColumnProperties =
        std::tuple<std::string, std::string, size_t, size_t>;

    std::array<std::vector<std::string>, 4> rows;
    std::vector<ColumnProperties> columnProperties;
    std::string cards  = "";
    bool needsRerender = false;

    size_t size = 0;

    size_t fieldLabelWidth        = 4;
    size_t fieldWidth             = 8;
    size_t contentWidth           = fieldLabelWidth + fieldWidth;
    size_t defaultSerperatorWidth = 5;

    bool useBorder                = false;
    std::string borderTop         = "\u2550";
    std::string borderRight       = "\u2551";
    std::string borderBottom      = "\u2550";
    std::string borderLeft        = "\u2551";
    std::string cornerTopLeft     = "\u2554";
    std::string cornerTopRight    = "\u2557";
    std::string cornerBottomRight = "\u255d";
    std::string cornerBottomLeft  = "\u255a";

    bool useLabels = false;
    std::vector<std::string> _labels;

    std::vector<std::array<std::string, 4>> _seperators;

    void addCard(const PowerplantCard* powerplant);

    void processLabels();
    void processSeperators();
    std::string getSeperatorString(size_t i, size_t j) const;

    void renderTopBorder(std::stringstream& builder);
    void renderContent(std::stringstream& builder);
    void renderBottomBorder(std::stringstream& builder);
    void render();

  public:
    void border(bool use);
    void labels(std::vector<std::string>& labels);
    void labels(std::vector<std::string>&& labels);
    void seperators(std::vector<std::array<std::string, 4>>& serperators);

    void operator()(const PowerplantCard* powerplant);

    // More specific than iterator overload
    void operator()(const PowerplantCard* powerplant1,
                    const PowerplantCard* powerlant2);

    template <typename... Ts>
    typename std::enable_if<
        conjunction<std::is_convertible<Ts, const PowerplantCard*>...>::value,
        void>::type
    operator()(const PowerplantCard*, const Ts... powerplants);

    template <typename Iterator>
    typename std::enable_if<is_iterator<Iterator>::value, void>::type
    operator()(Iterator first, Iterator last);

    friend std::ostream& operator<<(std::ostream& out, ArrangeCards& rhs);
};

template <typename... Ts>
typename std::enable_if<
    conjunction<std::is_convertible<Ts, const PowerplantCard*>...>::value,
    void>::type
ArrangeCards::operator()(const PowerplantCard* powerplant,
                         const Ts... powerplants)
{
    ++size;
    addCard(powerplant);
    operator()(powerplants...);
}

template <typename Iterator>
typename std::enable_if<is_iterator<Iterator>::value, void>::type
ArrangeCards::operator()(Iterator first, Iterator last)
{
    size += last - first;

    for (; first != last; ++first) {
        addCard(*first);
    }

    render();
    needsRerender = false;
}

#endif
