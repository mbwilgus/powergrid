#ifndef __DISCARD_VIEW_H__
#define __DISCARD_VIEW_H__

#include "Game.h"
#include "Player.h"
#include "View.h"

#include <string>

class DiscardView : public View
{
  private:
    Player* currentPlayer;

    void print();
    size_t input();
    bool confirm(size_t index);

  protected:
    virtual Game* getSubject() const override;

  public:
    DiscardView(Game* game);
    virtual void update() override;
    virtual void show() override;
};

#endif
