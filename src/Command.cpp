#include "Command.h"
#include "AuctionHouse.h"
#include "Cart.h"
#include "Market.h"

Command::~Command() {}

SelectPowerplant::SelectPowerplant(PowerplantMarket* receiver, size_t arg)
    : receiver(receiver), arg(arg)
{
}

void SelectPowerplant::execute()
{
    if (arg > 0) {
        receiver->select(arg - 1);
    }
}

DiscardPowerplant::DiscardPowerplant(Player* reciever, size_t arg)
    : reciever(reciever), arg(arg)
{
}

void DiscardPowerplant::execute()
{
    if (arg > 0) {
        reciever->discard(arg - 1);
    }
}

PlaceBid::PlaceBid(AuctionHouse* receiver, size_t arg)
    : receiver(receiver), arg(arg)
{
}

void PlaceBid::execute() { receiver->placeBid(arg); }

BuyResources::BuyResources(ResourceMarket* reciever, Cart& arg)
    : receiver(reciever), arg(arg)
{
}

void BuyResources::execute() { receiver->queue(arg); }
