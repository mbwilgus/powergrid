#include "Commodity.h"
#include "ResourceType.h"

#include <algorithm>
#include <cmath> // For floor
#include <ios>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility> // For swap

Commodity::Commodity() {}

Commodity::Commodity(const ResourceType& resource, size_t supply,
                     size_t poolSize, size_t volatility)
    : resource(resource), supply(supply), poolSize(poolSize),
      pool(poolSize - supply), volatility(volatility)
{
    forcaster = new DefaultForcaster(
        ResourceTypes::getProperty(resource, Property::scarcityThreshold));
}

Commodity::Commodity(const Commodity& source)
    : resource(source.resource), supply(source.supply),
      poolSize(source.poolSize), pool(source.pool),
      volatility(source.volatility)
{
    forcaster = source.forcaster->clone();
}

Commodity::~Commodity()
{
    delete forcaster;
    forcaster = nullptr;
}

void Commodity::take(size_t amount)
{
    if (amount > supply) {
        throw std::runtime_error("Cannot take");
    }
    supply -= amount;
}

void Commodity::putBack(size_t amount) { supply += amount; }

void Commodity::stock(size_t amount)
{
    amount = std::min(pool, amount);
    supply += amount;
    pool -= amount;
}

void Commodity::recycle(size_t amount) { pool += amount; }

size_t Commodity::currentMarketPrice() const
{
    return forcaster->forcast(supply, poolSize, volatility);
}

size_t Commodity::quote(size_t amount) const
{
    size_t cost = 0;
    for (size_t i = 0; i < amount; i++) {
        cost += forcaster->forcast(supply - i, poolSize, volatility);
    }
    return cost;
}

Commodity& Commodity::operator=(Commodity source)
{
    swap(*this, source);
    return *this;
}

Commodity::operator ResourceType() const { return resource; }

Forcaster::Forcaster() {}

Forcaster::Forcaster(size_t threshold) : threshold(threshold) {}

Forcaster::Forcaster(const Forcaster& source) : threshold(source.threshold) {}

Forcaster::~Forcaster() {}

size_t Forcaster::getThreshold() const { return threshold; }

DefaultForcaster::DefaultForcaster() {}

DefaultForcaster::DefaultForcaster(size_t threshold) : Forcaster(threshold) {}

DefaultForcaster::DefaultForcaster(const DefaultForcaster& source)
    : Forcaster(source)
{
}

DefaultForcaster::~DefaultForcaster() {}

DefaultForcaster* DefaultForcaster::clone() const
{
    return new DefaultForcaster(*this);
}

size_t DefaultForcaster::forcast(size_t supply, size_t poolSize,
                                 size_t volatility) const
{
    size_t basePrice = floor((poolSize + volatility - supply) / volatility);
    if (supply <= getThreshold()) {
        return getThreshold() - supply + 1 + basePrice;
    } else {
        return basePrice;
    }
}

void swap(Commodity& first, Commodity& second)
{
    using std::swap;

    swap(first.resource, second.resource);
    swap(first.supply, second.supply);
    swap(first.poolSize, second.poolSize);
    swap(first.pool, second.pool);
    swap(first.volatility, second.volatility);
    swap(first.forcaster, second.forcaster);
}

#define EMPTY_BLOCK '_'

std::ostream& operator<<(std::ostream& out, const Commodity& commodity)
{
    auto speculate = [&commodity](size_t supply) {
        size_t poolSize   = commodity.poolSize;
        size_t volatility = commodity.volatility;
        return commodity.forcaster->forcast(supply, poolSize, volatility);
    };

    char token = commodity.resource.name()[0];

    size_t supply     = commodity.supply;
    size_t poolSize   = commodity.poolSize;
    size_t volatility = commodity.volatility;

    size_t blocks             = poolSize / volatility;
    size_t numX               = poolSize - supply;
    size_t xBlocks            = ceil((double)numX / volatility);
    size_t tokenBlocks        = ceil((double)supply / volatility);
    size_t numXlastBlock      = numX % volatility;
    size_t numTokenFirstBlock = volatility - numXlastBlock;

    if (numXlastBlock == 0 && numX != 0) {
        numXlastBlock = volatility;
    }

    size_t maxPrice   = speculate(1);
    size_t blockWidth = std::max((size_t)(log10(maxPrice) + 1), volatility);

    for (size_t i = 0; i < blocks; i++) {
        size_t label   = speculate(poolSize - i * volatility);
        out.width(blockWidth);
        out << std::right << label << "\\";
    }

    out << "\n";

    size_t padding = blockWidth - volatility;

    if (xBlocks == 0) {
        xBlocks++;
    }
    for (size_t i = 0; i < xBlocks - 1; i++) {
        out.width(blockWidth);
        out << std::string(volatility, EMPTY_BLOCK) << '|';
    }

    if (numXlastBlock > 0) {
        out << std::string(padding, ' ')
            << std::string(numXlastBlock, EMPTY_BLOCK);

        if (numXlastBlock == volatility) {
            out << '|';
        }
    }

    if (tokenBlocks > 0) {
        out << std::string(padding, ' ')
            << std::string(numTokenFirstBlock, token) << '|';
    }

    if (tokenBlocks == 0) {
        tokenBlocks++;
    }
    for (size_t i = 0; i < tokenBlocks - 1; i++) {
        out.width(blockWidth);
        out << std::string(volatility, token) << '|';
    }

    return out;
}
