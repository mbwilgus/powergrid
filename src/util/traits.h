#ifndef __TRAITS_H__
#define __TRAITS_H__

#include <iterator>
#include <type_traits>

template <bool...> struct bool_pack {
};

template <typename... Ts>
using conjunction =
    std::is_same<bool_pack<true, Ts::value...>, bool_pack<Ts::value..., true>>;

template <typename...> using void_t = void;

template <typename T, typename Enable = void>
struct is_iterator : std::false_type {
};

// Standard iterators define these traits as a consistent interface to the
// underlying data
template <typename T>
struct is_iterator<T,
                   void_t<typename std::iterator_traits<T>::iterator_category,
                          typename std::iterator_traits<T>::value_type,
                          typename std::iterator_traits<T>::difference_type,
                          typename std::iterator_traits<T>::pointer,
                          typename std::iterator_traits<T>::reference>>
    : std::true_type {
};

#endif
