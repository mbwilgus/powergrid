#include "util.h"

#include <cstdlib>
#include <cstring>
#include <term.h>
#include <unistd.h> // POSIX compliant functionalities (might not be available on all systems)

char* head_of_path(const char* path)
{
    int cursor = 0;
    int tail   = -1;

    while (path[cursor++] != '\0') {
        if (path[cursor - 1] == '/') {
            tail = cursor - 1;
        }
    }

    if (tail > -1) {
        char* head = (char*)malloc(sizeof(char) *
                                   (tail + 1)); // +1 for the null terminator
        if (head) {
            memcpy(head, path, tail);
            head[tail] = '\0';
            return head;
        } else {
            return 0;
        }
    }

    return (char*)path;
}

int chdir_bin_directory(const char* path)
{
    char* head = head_of_path(path);

    int success = -1;

    if (head) {
        if (strcmp(head, ".") !=
            0) { // strcmp() returns 0 when strings are equal
            success = chdir(head);
        } else {
            success = 0;
        }

        free(head); // Be consistent (we used malloc() to create this
                    // pointer... so free() it)
        head = 0;
    }

    return success;
}

void clear_term()
{
    if (!cur_term) {
        int result;
        setupterm(0, STDOUT_FILENO, &result);
        if (result <= 0)
            return;
    }

    putp(tigetstr("clear"));
}
